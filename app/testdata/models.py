# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from app.Base import Base

# Define a User model
class TestDataModel(Base):

    __tablename__ = 'test_data'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    Value    = db.Column(db.String(128),  nullable=False)
    ParamIndex    = db.Column(db.Integer,  nullable=False)
    Comments    = db.Column(db.String(1000),  nullable=True)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name
        self.ParamIndex = 1

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod    		
    def updateRecord(self, Info):
        try:
            testData = self.query.filter_by(id=Info['ID']).first()
            testData.name = Info['Name'];
            testData.Value = Info['Value'];
            testData.ParamIndex = Info['ParamIndex'];
            testData.Comments = Info['Comments'];
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0
    @classmethod
    def addRecord(self, data):
        try:
            testData = self(name = data['Name'])
            testData.Value = data['Value'];
            testData.ParamIndex = data['ParamIndex'];
            testData.Comments = data['Comments'];
            db.session.add(testData)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0