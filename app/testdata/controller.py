from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.testdata.models import TestDataModel

testdata = Blueprint('testdata', __name__, url_prefix='/testdata')
		
@testdata.route("/", methods=['GET', 'POST'])
def showlist():
	testDataList = list(TestDataModel.getallActiveRecords())
	return render_template("testdata/list.html", testData = testDataList);
	
@testdata.route("/edit/<ID>", methods=['GET', 'POST'])
def edit(ID):
	if request.method == "POST":
		details = request.form
		data = TestDataModel.updateRecord(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('testdata.showlist'))
		else:
			info = TestDataModel.getRecordByID(ID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("testdata/edit.html", testdata = info);
	info = TestDataModel.getRecordByID(ID)
	return render_template("testdata/edit.html", testdata = info);

@testdata.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = TestDataModel.addRecord(details)
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('testdata.showlist'))
		else:
			info = {'name': details['Name'], 'Value': details['Value'], 'ParamIndex': details['ParamIndex'], 'Comments': details['Comments']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("testdata/add.html", testdata = info);
	info = {'name': ''}
	return render_template("testdata/add.html", testdata = info);
	
@testdata.route("/delete/<ID>", methods=['GET'])
def delete(ID):
	data = TestDataModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('testdata.showlist'))
	