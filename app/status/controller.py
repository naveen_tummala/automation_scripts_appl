from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.status.models import StatusModel

status = Blueprint('status', __name__, url_prefix='/status')
		
@status.route("/", methods=['GET', 'POST'])
def showlist():
	statuslist = list(StatusModel.getallActiveRecords())
	for i in statuslist:
		pprint(vars(i))
	return render_template("status/list.html", statusses = statuslist);
	
@status.route("/edit/<statusID>", methods=['GET', 'POST'])
def edit(statusID):
	if request.method == "POST":
		details = request.form
		data = StatusModel.updateStatus(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('status.showlist'))
		else:
			statusInfo = StatusModel.getRecordByID(statusID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("status/edit.html", status = statusInfo);
	statusInfo = StatusModel.getRecordByID(statusID)
	#for i in statusInfo:
	#	pprint(vars(i))
	return render_template("status/edit.html", status = statusInfo);

@status.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = StatusModel.addStatus(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('status.showlist'))
		else:
			statusInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("status/add.html", status = statusInfo);
	statusInfo = {'name': ''}
	return render_template("status/add.html", status = statusInfo);
	
@status.route("/delete/<statusID>", methods=['GET'])
def delete(statusID):
	data = StatusModel.deleteRecord(statusID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('status.showlist'))
	