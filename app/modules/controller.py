from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.modules.models import ModulesModel

modules = Blueprint('modules', __name__, url_prefix='/modules')
		
@modules.route("/", methods=['GET', 'POST'])
def showlist():
	moduleslist = list(ModulesModel.getallModules())
	return render_template("modules/list.html", modules = moduleslist);
	
@modules.route("/edit/<moduleID>", methods=['GET', 'POST'])
def edit(moduleID):
	if request.method == "POST":
		details = request.form
		data = ModulesModel.updateModule(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('modules.showlist'))
		else:
			moduleInfo = ModulesModel.getModuleByID(moduleID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("modules/edit.html", module = moduleInfo);
	moduleInfo = ModulesModel.getModuleByID(moduleID)
	#for i in moduleInfo:
	#	pprint(vars(i))
	return render_template("modules/edit.html", module = moduleInfo);

@modules.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = ModulesModel.addModule(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('modules.showlist'))
		else:
			moduleInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("modules/add.html", module = moduleInfo);
	moduleInfo = {'name': ''}
	return render_template("modules/add.html", module = moduleInfo);
	
@modules.route("/delete/<moduleID>", methods=['GET'])
def delete(moduleID):
	data = ModulesModel.deleteModule(moduleID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('modules.showlist'))
	