# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    Created_date = db.Column(db.DateTime,  default=db.func.current_timestamp())
    Modified_date = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

# Define a User model
class ModulesModel(Base):

    __tablename__ = 'modules'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallModules(self):
        print(self)
		#order_by = re.sub('[^0-9a-zA-Z]+', '', order_by) + desc
        return self.query.filter_by(active = 1).order_by(self.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getModuleByID(self, moduleID):
        return self.query.filter_by(id = moduleID).first()
	
    @classmethod    		
    def updateModule(self, moduleInfo):
        print(moduleInfo)
        try:
            model = self.query.filter_by(id=moduleInfo['moduleID']).first()
            model.name = moduleInfo['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def deleteModule(self , moduleID):
        try:
            model = self.query.filter_by(id=moduleID).first()
            model.active = 0
            db.session.commit()
            #module = self.query.filter_by(id = moduleID).first()
            #db.session.delete(module)
            #db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def addModule(self, moduleName):
        print(moduleName)
        try:
            module = self(name = moduleName)
            db.session.add(module)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0