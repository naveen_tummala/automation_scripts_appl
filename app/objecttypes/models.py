# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    Created_date = db.Column(db.DateTime,  default=db.func.current_timestamp())
    Modified_date = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

# Define a User model
class ObjecttypesModel(Base):

    __tablename__ = 'object_types'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallObjectTypes(self):
        return self.query.filter_by(active = 1).order_by(self.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getObjectTypeByID(self, objectTypeID):
        return self.query.filter_by(id = objectTypeID).first()
	
    @classmethod    		
    def updateObjectType(self, objectTypeInfo):
        try:
            model = self.query.filter_by(id=objectTypeInfo['objectTypeID']).first()
            model.name = objectTypeInfo['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def deleteObjectType(self , objectTypeID):
        try:
            model = self.query.filter_by(id=objectTypeID).first()
            model.active = 0
            db.session.commit()
            #objectType = self.query.filter_by(id = objectTypeID).first()
            #db.session.delete(objectType)
            #db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0
    @classmethod
    def addObjectType(self, Name):
        try:
            ObjectType = self(name = Name)
            db.session.add(ObjectType)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0