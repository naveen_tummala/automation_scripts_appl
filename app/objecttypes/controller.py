from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.objecttypes.models import ObjecttypesModel

objecttypes = Blueprint('objecttypes', __name__, url_prefix='/objecttypes')
		
@objecttypes.route("/", methods=['GET', 'POST'])
def showlist():
	objectTypes = list(ObjecttypesModel.getallObjectTypes())
	return render_template("objecttypes/list.html", objecttypes = objectTypes);
	
@objecttypes.route("/edit/<objecttypeID>", methods=['GET', 'POST'])
def edit(objecttypeID):
	if request.method == "POST":
		details = request.form
		data = ObjecttypesModel.updateObjectType(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('objecttypes.showlist'))
		else:
			objectTypeInfo = ObjecttypesModel.getObjectTypeByID(objecttypeID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("objecttypes/edit.html", objectType = objectTypeInfo);
	objectTypeInfo = ObjecttypesModel.getObjectTypeByID(objecttypeID)
	#for i in objectTypeInfo:
	#	pprint(vars(i))
	return render_template("objecttypes/edit.html", objectType = objectTypeInfo);

@objecttypes.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = ObjecttypesModel.addObjectType(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('objecttypes.showlist'))
		else:
			objectTypeInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("objecttypes/add.html", objectType = objectTypeInfo);
	objectTypeInfo = {'name': ''}
	return render_template("objecttypes/add.html", objectType = objectTypeInfo);
	
@objecttypes.route("/delete/<objecttypeID>", methods=['GET'])
def delete(objecttypeID):
	data = ObjecttypesModel.deleteObjectType(objecttypeID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('objecttypes.showlist'))
	