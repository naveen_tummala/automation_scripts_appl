from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.browse.models import BrowseModel

browse = Blueprint('browse', __name__, url_prefix='/browse')
		
@browse.route("/", methods=['GET', 'POST'])
def showlist():
	browselist = list(BrowseModel.getallBrowse())
	return render_template("browse/list.html", browse = browselist);
	
@browse.route("/edit/<browseID>", methods=['GET', 'POST'])
def edit(browseID):
	if request.method == "POST":
		details = request.form
		data = BrowseModel.updateBrowse(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('browse.showlist'))
		else:
			browseInfo = BrowseModel.getBrowseByID(browseID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("browse/edit.html", browse = browseInfo);
	browseInfo = BrowseModel.getBrowseByID(browseID)
	#for i in browseInfo:
	#	pprint(vars(i))
	return render_template("browse/edit.html", browse = browseInfo);

@browse.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = BrowseModel.addBrowse(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('browse.showlist'))
		else:
			browseInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("browse/add.html", browse = browseInfo);
	browseInfo = {'name': ''}
	return render_template("browse/add.html", browse = browseInfo);
	
@browse.route("/delete/<browseID>", methods=['GET'])
def delete(browseID):
	data = BrowseModel.deleteBrowse(browseID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('browse.showlist'))
	