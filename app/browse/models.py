# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from app.Base import Base

# Define a User model
class BrowseModel(Base):

    __tablename__ = 'browse'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallBrowse(self):
        print(self)
		#order_by = re.sub('[^0-9a-zA-Z]+', '', order_by) + desc
        return self.query.filter_by(active = 1).order_by(self.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getBrowseByID(self, browseID):
        return self.query.filter_by(id = browseID).first()
	
    @classmethod    		
    def updateBrowse(self, browseInfo):
        print(browseInfo)
        try:
            model = self.query.filter_by(id=browseInfo['browseID']).first()
            model.name = browseInfo['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def deleteBrowse(self , browseID):
        try:
            model = self.query.filter_by(id=browseID).first()
            model.active = 0
            db.session.commit()
            #browse = self.query.filter_by(id = browseID).first()
            #db.session.delete(browse)
            #db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def addBrowse(self, browseName):
        print(browseName)
        try:
            browse = self(name = browseName)
            db.session.add(browse)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0