# Import flask and template operators
from flask import Flask, render_template,g, session, redirect, url_for, request
from pprint import pprint
import os
from array import array
loginnotrequired = [ "/users/login", "/users/logout"]
moduleName = ''

# Import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
from flask_mysqldb import MySQL

# Define the WSGI application object
app = Flask(__name__)
app.secret_key = os.urandom(24)
# Configurations
#app.config.from_object('config')
app.config.from_object('config.development')

mysql = MySQL(app)

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
@app.before_request
def before_request():
	#print(request.path)
	arr = request.path.split("/");
	#global moduleName;
	session['moduleName'] = arr[1];
	#print(moduleName)
	#print(arr);
	global loginnotrequired
	#print( __name__ )
	if 'username' in session:
		if request.path == '/users/login':
			return redirect(url_for('users.showhome'))
	else:
		if request.path not in loginnotrequired and '/static' not in request.path:
			print('loginrequired')
			return redirect(url_for('users.showsingup'))

# Import a module / component using its blueprint handler variable (mod_auth)
from app.users.controller import users as users
from app.modules.controller import modules as modules
from app.browse.controller import browse as browse
from app.priorities.controller import priorities as priorities
from app.keywords.controller import keywords as keywords
from app.objecttypes.controller import objecttypes as objecttypes
from app.objectrepo.controller import objectrepo as objectrepo
from app.status.controller import status as status
from app.window.controller import window as window
from app.testcasemaster.controller import testcasemaster as testcasemaster
from app.testdata.controller import testdata as testdata
from app.datasheet.controller import datasheet as datasheet
from app.settings.controller import settings as settings

# Register blueprint(s)
app.register_blueprint(users)
app.register_blueprint(modules)
app.register_blueprint(browse)
app.register_blueprint(priorities)
app.register_blueprint(keywords)
app.register_blueprint(objecttypes)
app.register_blueprint(objectrepo)
app.register_blueprint(status)
app.register_blueprint(window)
app.register_blueprint(testcasemaster)
app.register_blueprint(testdata)
app.register_blueprint(datasheet)
app.register_blueprint(settings)
# app.register_blueprint(xyz_module)
# ..

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()