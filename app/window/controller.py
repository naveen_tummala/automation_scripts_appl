from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.window.models import WindowModel

window = Blueprint('window', __name__, url_prefix='/window')
		
@window.route("/", methods=['GET', 'POST'])
def showlist():
	windowsList = list(WindowModel.getallActiveRecords())
	for i in windowsList:
		pprint(vars(i))
	return render_template("window/list.html", windows = windowsList);
	
@window.route("/edit/<ID>", methods=['GET', 'POST'])
def edit(ID):
	if request.method == "POST":
		details = request.form
		data = WindowModel.updateRecord(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('window.showlist'))
		else:
			info = WindowModel.getRecordByID(ID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("window/edit.html", window = info);
	info = WindowModel.getRecordByID(ID)
	#for i in info:
	#	pprint(vars(i))
	return render_template("window/edit.html", window = info);

@window.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = WindowModel.addRecord(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('window.showlist'))
		else:
			info = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("window/add.html", window = info);
	info = {'name': ''}
	return render_template("window/add.html", window = info);
	
@window.route("/delete/<ID>", methods=['GET'])
def delete(ID):
	data = WindowModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('window.showlist'))
	