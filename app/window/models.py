# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from app.Base import Base

# Define a User model
class WindowModel(Base):

    __tablename__ = 'windownames'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod    		
    def updateRecord(self, Info):
        print(Info)
        try:
            status = self.query.filter_by(id=Info['ID']).first()
            status.name = Info['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def addRecord(self, name):
        print(name)
        try:
            status = self(name = name)
            db.session.add(status)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0