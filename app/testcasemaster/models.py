# Import the database object (db) from the main application module
# Import base class 
from app import db
from app.Base import Base
from sqlalchemy import text
from flask import session
import sys

# Define a User model
class TestCaseMasterModel(Base):

    __tablename__ = 'test_case_master'

    # User Name  
    title          = db.Column(db.String(128),  nullable=False)
    testrailcaseno = db.Column(db.String(225), nullable=False)
    priorityid       = db.Column(db.Integer, primary_key=True)
    moduleid       = db.Column(db.Integer, primary_key=True)
    assigneduserid = db.Column(db.Integer, primary_key=True)
    status         = db.Column(db.Integer, primary_key=True)
    created_by      = db.Column(db.Integer, primary_key=True)
    modified_by     = db.Column(db.Integer, primary_key=True)

    # New instance instantiation procedure
    def __init__(self, title):

        self.title     = title

    def __repr__(self):
        return self #'<User %r>' % (self.title)

    @classmethod
    def getallActivetcmRecords(self, sortby = 'tcm.Created_date DESC'):
        sql = text("""SELECT 
                    tcm.*,
                    p.Name as priorityName,
                    u.Name as userName,
                    m.Name as moduleName,
                    tcs.Name as testCaseStatus        
                    FROM test_case_master tcm
                    INNER JOIN priority p ON p.ID = tcm.PriorityID
                    INNER JOIN users u ON u.id = tcm.assignedUserID
                    INNER JOIN modules m ON m.ID = tcm.ModuleID
                    INNER JOIN test_case_status tcs ON tcs.ID = tcm.status
                    WHERE tcm.Active = 1
                    ORDER BY """ + sortby)
        return db.session.execute(sql)
	
    @classmethod    		
    def updatetcm(self, Info):
        print(Info)
        try:
            model = self.query.filter_by(id=Info['ID']).first()
            model.title = Info['title']
            model.moduleid = Info['moduleid']
            model.testrailcaseno = Info['testrailcaseno']
            model.priorityid = Info['priorityid']
            model.assigneduserid = Info['assigneduserid']
            model.status = Info['statusid']
            model.modified_by = session['user_id']
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0
   
    @classmethod
    def addtcm(self, Info):        
        try:
            model = self(title = Info['title'])
            model.moduleid = Info['moduleid']
            model.testrailcaseno = Info['testrailcaseno']
            model.priorityid = Info['priorityid']
            model.assigneduserid = Info['assigneduserid']
            model.status = Info['statusid']
            model.created_by = session['user_id']
            db.session.add(model)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0