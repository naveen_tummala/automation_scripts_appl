from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.testcasemaster.models import TestCaseMasterModel
from app.modules.models import ModulesModel
from app.priorities.models import PrioritiesModel
from app.users.models import UsersModel
from app.status.models import StatusModel

testcasemaster = Blueprint('testcasemaster', __name__, url_prefix='/testcasemaster')
		
@testcasemaster.route("/", methods=['GET', 'POST'])
def showlist():
	moduleslist = list(TestCaseMasterModel.getallActivetcmRecords())	
	return render_template("testcasemaster/list.html", itemsList = moduleslist);
	
@testcasemaster.route("/edit/<ID>", methods=['GET', 'POST'])
def edit(ID):
    priorites = PrioritiesModel.getallPriorities()
    modules   = ModulesModel.getallModules()
    users     = UsersModel.getallActiveRecords()
    status    = StatusModel.getallActiveRecords()
    if request.method == "POST":
        details = request.form
        data = TestCaseMasterModel.updatetcm(details)
        print(data)
        if data == 1:
            flash('Record Updated successfully.')
            return redirect(url_for('testcasemaster.showlist'))
        else:
            module = TestCaseMasterModel.getRecordByID(ID)
            pprint(vars(module))
            flash('ERROR!  unable to update the Record, please try later')
            return render_template("testcasemaster/form.html", **locals());
    module = TestCaseMasterModel.getRecordByID(ID)
    updatepage = True
    pprint(vars(module))
	#for i in moduleInfo:
	#	pprint(vars(i))
    return render_template("testcasemaster/form.html", **locals());

@testcasemaster.route("/add/", methods=['GET', 'POST'])
def add():
    priorites = PrioritiesModel.getallPriorities()
    modules   = ModulesModel.getallModules()
    users     = UsersModel.getallActiveRecords()
    status    = StatusModel.getallActiveRecords()
    if request.method == "POST":
        details = request.form
        print(details)
        data = TestCaseMasterModel.addtcm(details)		
        if data == 1:
            flash('Record Created successfully.')
            return redirect(url_for('testcasemaster.showlist'))
        else:
            module = {'title': details['title'], 'testrailcaseno':details['testrailcaseno'], 'priorityid':details['priorityid'], 'moduleid':details['moduleid'], 'assigneduserid':details['assigneduserid'], 'statusid':details['status']}
            flash('ERROR!  unable to add the Record, please try later')
            return render_template("testcasemaster/form.html", **locals());

    module = {'title': '', 'testrailcaseno':'', 'priorityid':'', 'moduleid':'', 'assigneduserid':'', 'statusid':''} 
    updatepage = False   
    return render_template("testcasemaster/form.html", **locals());
	
@testcasemaster.route("/delete/<ID>", methods=['GET'])
def delete(ID):
	data = TestCaseMasterModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('testcasemaster.showlist'))
	