# Define a base model for other database tables to inherit
from app import db
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    active    = db.Column(db.Integer,  default=1)
    Created_date = db.Column(db.DateTime,  default=db.func.current_timestamp())
    Modified_date = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

    @classmethod
    def getallActiveRecords(classObj):
        print(classObj)
		#order_by = re.sub('[^0-9a-zA-Z]+', '', order_by) + desc
        return classObj.query.filter_by(active = 1).order_by(classObj.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getRecordByID(classObj, id):
        return classObj.query.filter_by(id = id).first()

    @classmethod
    def deleteRecord(classObj , id):
        try:
            model = classObj.query.filter_by(id = id).first()
            model.active = 0
            db.session.commit()
            return 1
        except:
            return 0                                           