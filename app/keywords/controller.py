from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.keywords.models import KeywordsModel

keywords = Blueprint('keywords', __name__, url_prefix='/keywords')
		
@keywords.route("/", methods=['GET', 'POST'])
def showlist():
	keywordslist = list(KeywordsModel.getallKeywords())
	return render_template("keywords/list.html", keywords = keywordslist);
	
@keywords.route("/edit/<keywordID>", methods=['GET', 'POST'])
def edit(keywordID):
	if request.method == "POST":
		details = request.form
		data = KeywordsModel.updateKeyword(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('keywords.showlist'))
		else:
			keywordInfo = KeywordsModel.getKeywordByID(keywordID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("keywords/edit.html", keyword = keywordInfo);
	keywordInfo = KeywordsModel.getKeywordByID(keywordID)
	#for i in keywordInfo:
	#	pprint(vars(i))
	return render_template("keywords/edit.html", keyword = keywordInfo);

@keywords.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = KeywordsModel.addKeyword(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('keywords.showlist'))
		else:
			KeywordInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("keywords/add.html", keyword = KeywordInfo);
	KeywordInfo = {'name': ''}
	return render_template("keywords/add.html", keyword = KeywordInfo);
	
@keywords.route("/delete/<keywordID>", methods=['GET'])
def delete(keywordID):
	data = KeywordsModel.deleteKeyword(keywordID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('keywords.showlist'))
	