# Import the database object (db) from the main application Keywords
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from app.Base import Base

# Define a User model
class KeywordsModel(Base):

    __tablename__ = 'keywords'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallKeywords(self):
        print(self)
		#order_by = re.sub('[^0-9a-zA-Z]+', '', order_by) + desc
        return self.query.filter_by(active = 1).order_by(self.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getKeywordByID(self, keywordID):
        return self.query.filter_by(id = keywordID).first()
	
    @classmethod    		
    def updateKeyword(self, keywordInfo):
        print(keywordInfo)
        try:
            keyword = self.query.filter_by(id=keywordInfo['keywordID']).first()
            keyword.name = keywordInfo['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def deleteKeyword(self , keywordID):
        try:
            model = self.query.filter_by(id=keywordID).first()
            model.active = 0
            db.session.commit()
            #keyword = self.query.filter_by(id = keywordID).first()
            #db.session.delete(keyword)
            #db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def addKeyword(self, KeywordName):
        print(KeywordName)
        try:
            keyword = self(name = KeywordName)
            db.session.add(keyword)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0