from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from app import db
from pprint import pprint

from app.users.models import UsersModel

users = Blueprint('users', __name__, url_prefix='/users')
@users.route("/main")
def main():
	'''
	cur = mysql.connection.cursor()
	cur.execute("CREATE TABLE `users` (`id` int(11) NOT NULL, `Name` varchar(100) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL)")
	mysql.connection.commit()
	cur.close()
	'''
	return 'Success'
    #return render_template('index.html')
	
@users.route('/login', methods=['GET', 'POST'])
def showsingup():
	pprint(vars(session))
	if request.method == "POST":
		details = request.form
		userName = details['inputEmail']
		password = details['inputPassword']
		#print(details);
		userinfo = UsersModel.query.filter_by(username=userName, password=password).first()
		#print(type(userinfo))
		#print(userinfo.username)
		#userinfo = UsersModel.query.filter(UsersModel.username.endswith('@valuelabs.com')).first()
		try:
			if userinfo.id:
				flash('Logged in successfully.')
				session['username'] = userinfo.username
				session['user_id']  = userinfo.id
				pprint(vars(session))
				return render_template('users/index.html', record=userinfo)
		except:
			flash('ERROR! Incorrect login credentials.')
			render_template('users/login.html')	
	return render_template('users/login.html')	

@users.route("/", methods=['GET', 'POST'])
def showhome():
	username = session['username']
	return render_template('users/home.html', data = username)

@users.route('/logout')
def logout():
	session.clear()
	return redirect(url_for('users.showsingup'))
	
@users.route("/list", methods=['GET', 'POST'])
def showlist():
	usersList = list(UsersModel.getallActiveRecords())
	return render_template("users/list.html", users = usersList)
	
@users.route("/edit/<ID>", methods=['GET', 'POST'])
def edit(ID):
	if request.method == "POST":
		details = request.form
		print(details)
		data = UsersModel.updateRecord(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('users.showlist'))
		else:
			info = UsersModel.getRecordByID(ID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("users/edit.html", userdata = info);
	info = UsersModel.getRecordByID(ID)
	return render_template("users/edit.html", userdata = info);

@users.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = UsersModel.addRecord(details)
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('users.showlist'))
		else:
			info = {'name': details['Name'], 'username': details['Email'], 'password': details['Password']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("users/add.html", userdata = info);
	info = {'name': '', 'username': '', 'password': ''}
	return render_template("users/add.html", userdata = info);
	
@users.route("/delete/<ID>", methods=['GET'])
def delete(ID):
	data = UsersModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('users.showlist'))