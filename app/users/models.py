from app import db
from app.Base import Base 
from sqlalchemy import text
from flask import session
import sys 

# Define a User model
class UsersModel(Base):

    __tablename__ = 'users'

    
    # Identification Data: email & password
    name        = db.Column(db.String(100),  nullable=False,
                                            unique=True)
    username    = db.Column(db.String(255),  nullable=False,
                                            unique=True)
    password = db.Column(db.String(255),  nullable=False)
    role_id    = db.Column(db.Integer,  nullable=True)
    created_by      = db.Column(db.Integer, nullable=True)
    modified_by     = db.Column(db.Integer, nullable=True)

   
    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name
        self.role_id = 8;
        #self.password = password

    def __repr__(self):
        return self #'<User %r>' % (self.username)

    @classmethod    		
    def updateRecord(self, Info):
        try:
            user = self.query.filter_by(id=Info['ID']).first()
            user.name = Info['Name'];
            if Info['Password'] != '':
                user.password = Info['Password']
            user.username = Info['Email'];
            user.modified_by = session['user_id']
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0
    @classmethod
    def addRecord(self, data):
        try:
            user = self(name = data['Name'])
            user.password = data['Password'];
            user.username = data['Email'];
            user.role_id = 8;
            user.created_by = session['user_id']
            user.modified_by = session['user_id']
            db.session.add(user)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info())
            return 0		