from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.settings.models import SettingsModel

settings = Blueprint('settings', __name__, url_prefix='/settings')
		
@settings.route("/", methods=['GET', 'POST'])
def showhome():
	appName = SettingsModel.getAppName()
	return render_template("settings/home.html", **locals());
	
@settings.route("/editAppName", methods=['GET', 'POST'])
def editAppName():
	if request.method == "POST":
		details = request.form
		data = SettingsModel.updateAppName(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('settings.showhome'))
		else:
			appNameInfo = SettingsModel.getAppName()
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("settings/editAppName.html", **locals());
	appNameInfo = SettingsModel.getAppName()
	#for i in objectTypeInfo:
	#	pprint(vars(i))
	return render_template("settings/editAppName.html", **locals());

@settings.route("/flushtestdata/", methods=['GET', 'POST'])
def flushtestdata():
	print('flush')
	success = SettingsModel.flushTestData()
	print(success)
	if success == 1:
		flash('Test Data Deleted successfully.')
	else:
		flash('ERROR!  unable to delete the Test data, please try later')
	return redirect(url_for('settings.showhome'))
	
@settings.route("/delete/<objecttypeID>", methods=['GET'])
def delete(objecttypeID):
	data = ObjecttypesModel.deleteObjectType(objecttypeID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('objecttypes.showlist'))
	