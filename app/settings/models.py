# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from sqlalchemy import text, Column, String, Integer

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    Modified_by   = db.Column(db.Integer, nullable=True)
    Modified_date = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

# Define a User model
class SettingsModel(Base):

    __tablename__ = 'application'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
    def __str__(self):
        return str(self.name)
    @classmethod
    def getAppName(self):
        return self.query.filter_by(id = 1).first()
    
    
    @classmethod    		
    def updateAppName(self, Info):
        try:
            app = self.query.filter_by(id=Info['ID']).first()
            app.name = Info['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    
    @classmethod
    def flushTestData(self):
        try:
            sql = text("""TRUNCATE Table data_sheet;
			TRUNCATE Table test_case_master;
			TRUNCATE Table windownames;
			TRUNCATE Table browse;
			TRUNCATE Table keywords;
			TRUNCATE Table test_data;
			TRUNCATE Table object_repo;
			TRUNCATE Table object_types;
			TRUNCATE Table test_case_status;
			TRUNCATE Table status;
			TRUNCATE Table modules;
			TRUNCATE Table priority;
			""")
            db.session.execute(sql)
            return 1
        except:
            print("Unexpected error:", sys.exc_info())
            return 0