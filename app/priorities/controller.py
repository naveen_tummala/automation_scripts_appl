from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.priorities.models import PrioritiesModel

priorities = Blueprint('priorities', __name__, url_prefix='/priorities')
		
@priorities.route("/", methods=['GET', 'POST'])
def showlist():
	prioritieslist = list(PrioritiesModel.getallPriorities())
	return render_template("priorities/list.html", priorities = prioritieslist);
	
@priorities.route("/edit/<priorityID>", methods=['GET', 'POST'])
def edit(priorityID):
	if request.method == "POST":
		details = request.form
		data = PrioritiesModel.updatePriority(details)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('priorities.showlist'))
		else:
			priorityInfo = PrioritiesModel.getPriorityByID(priorityID)
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("priorities/edit.html", priority = priorityInfo);
	priorityInfo = PrioritiesModel.getPriorityByID(priorityID)
	#for i in priorityInfo:
	#	pprint(vars(i))
	return render_template("priorities/edit.html", priority = priorityInfo);

@priorities.route("/add/", methods=['GET', 'POST'])
def add():
	if request.method == "POST":
		details = request.form
		data = PrioritiesModel.addPriority(details['Name'])		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('priorities.showlist'))
		else:
			priorityInfo = {'name': details['Name']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("priorities/add.html", priority = priorityInfo);
	priorityInfo = {'name': ''}
	return render_template("priorities/add.html", priority = priorityInfo);
	
@priorities.route("/delete/<priorityID>", methods=['GET'])
def delete(priorityID):
	data = PrioritiesModel.deletePriority(priorityID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('priorities.showlist'))
	