# Import the database object (db) from the main application Priority
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    Created_date = db.Column(db.DateTime,  default=db.func.current_timestamp())
    Modified_date = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

# Define a User model
class PrioritiesModel(Base):

    __tablename__ = 'priority'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    active    = db.Column(db.Integer,  default=1)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallPriorities(self):
        print(self)
		#order_by = re.sub('[^0-9a-zA-Z]+', '', order_by) + desc
        return self.query.filter_by(active = 1).order_by(self.name.asc()).all() #return self.query.order_by(self.Name).all()
    
    @classmethod    		
    def getPriorityByID(self, priorityID):
        return self.query.filter_by(id = priorityID).first()
	
    @classmethod    		
    def updatePriority(self, priorityInfo):
        print(priorityInfo)
        try:
            priority = self.query.filter_by(id=priorityInfo['priorityID']).first()
            priority.name = priorityInfo['Name'];
            db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def deletePriority(self , priorityID):
        try:
            model = self.query.filter_by(id=priorityID).first()
            model.active = 0
            db.session.commit()
            #priority = self.query.filter_by(id = priorityID).first()
            #db.session.delete(priority)
            #db.session.commit()
            return 1
        except:
            return 0
    @classmethod
    def addPriority(self, priorityName):
        print(priorityName)
        try:
            priority = self(name = priorityName)
            db.session.add(priority)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0