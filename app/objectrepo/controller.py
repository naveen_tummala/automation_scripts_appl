from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for
from flask import current_app as app
from app import db
from pprint import pprint

from app.objectrepo.models import ObjectRepoModel
from app.objecttypes.models import ObjecttypesModel

objectrepo = Blueprint('objectrepo', __name__, url_prefix='/objectrepo')
		
@objectrepo.route("/", methods=['GET', 'POST'])
def showlist():
	moduleslist = list(ObjectRepoModel.getallActivetcmRecords())
	return render_template("objectrepo/list.html", itemsList = moduleslist);
	
@objectrepo.route("/edit/<ID>", methods=['GET', 'POST'])
def edit(ID):
	objecttypes     = ObjecttypesModel.getallObjectTypes()
	if request.method == "POST":
		details = request.form
		data = ObjectRepoModel.updateRepo(details)
		print(data)
		if data == 1:
			flash('Record Updated successfully.')
			return redirect(url_for('objectrepo.showlist'))
		else:
			module = ObjectRepoModel.getRecordByID(ID)
			pprint(vars(moduleInfo))
			flash('ERROR!  unable to update the Record, please try later')
			return render_template("objectrepo/edit.html", module = record);
	module = ObjectRepoModel.getRecordByID(ID)
	#for i in moduleInfo:
	#	pprint(vars(i))
	return render_template("objectrepo/edit.html", **locals());

@objectrepo.route("/add/", methods=['GET', 'POST'])
def add():
	objecttypes     = ObjecttypesModel.getallObjectTypes()
	if request.method == "POST":
		details = request.form
		data = ObjectRepoModel.addRepo(details)		
		if data == 1:
			flash('Record Created successfully.')
			return redirect(url_for('objectrepo.showlist'))
		else:
			module = {'name': details['Name'], 'Type': details['Type']}
			flash('ERROR!  unable to add the Record, please try later')
			return render_template("objectrepo/add.html", **locals());
	module = {'name': '', 'locator':''}
	return render_template("objectrepo/add.html", **locals());
	
@objectrepo.route("/delete/<ID>", methods=['GET'])
def delete(ID):
	data = ObjectRepoModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('objectrepo.showlist'))
	