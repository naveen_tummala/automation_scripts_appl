# Import the database object (db) from the main application module
# Import base class 
from app import db
import sys
from app.Base import Base
from sqlalchemy import text

# Define a User model
class ObjectRepoModel(Base):

    __tablename__ = 'object_repo'

    # User Name
    name    = db.Column(db.String(128),  nullable=False)
    locator = db.Column(db.String(225), nullable=False)
    Type    = db.Column(db.Integer,  nullable = False)

    # New instance instantiation procedure
    def __init__(self, name):

        self.name     = name

    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallActivetcmRecords(self):
        sql = text("""SELECT 
                    orp.*,
                    ot.Name as TypeName			
                    FROM object_repo orp
                    INNER JOIN object_types ot ON ot.ID = orp.Type
					Where orp.Active = 1
                    ORDER BY orp.Name""")
        return db.session.execute(sql)
	
    @classmethod 		
    def updateRepo(self, moduleInfo):
        print(moduleInfo)
        try:
            model = self.query.filter_by(id=moduleInfo['ID']).first()
            model.name = moduleInfo['Name'];
            model.Type = moduleInfo['Type'];
            model.locator = moduleInfo['Locator'];
            db.session.commit()
            return 1
        except:
            return 0
   
    @classmethod
    def addRepo(self, Info):        
        try:
            module = self(name = Info['Name'])
            module.locator = Info['Locator'];
            module.Type = Info['Type'];
            db.session.add(module)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0