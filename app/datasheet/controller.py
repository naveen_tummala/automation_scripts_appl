from flask import Flask, Blueprint, render_template, request, session, redirect, flash, url_for, json
from flask import current_app as app
from app import db
from pprint import pprint

from app.datasheet.models import DataSheetModel
from app.browse.models import BrowseModel
from app.keywords.models import KeywordsModel
from app.objectrepo.models import ObjectRepoModel
from app.testdata.models import TestDataModel
from app.testcasemaster.models import TestCaseMasterModel
from app.window.models import WindowModel

datasheet = Blueprint('datasheet', __name__, url_prefix='/datasheet')
@datasheet.route('/<ID>', defaults={'ID': None})
@datasheet.route("/<ID>", methods=['GET', 'POST'])
def showlist(ID):
    testcasemaster = TestCaseMasterModel.getallActivetcmRecords('tcm.ID ASC')
    datasheet = list(DataSheetModel.getallActivetcmRecords(ID))
    totalCount = len(datasheet)
    if ID != 'None':
        testcaseID = int(ID)
    else:
        testcaseID = ID
        
    return render_template("datasheet/list.html", **locals());
		
@datasheet.route('/edit/<ID>/<testcaseID>"', defaults={'testcaseID': None})
@datasheet.route("/edit/<ID>/<testcaseID>", methods=['GET', 'POST'])
def edit(ID, testcaseID):
    browse = BrowseModel.getallActiveRecords()
    keywords   = KeywordsModel.getallActiveRecords()
    objectrepo     = ObjectRepoModel.getallActivetcmRecords()
    testdata     = TestDataModel.getallActiveRecords()
    testcasemaster    = TestCaseMasterModel.getallActivetcmRecords('tcm.ID ASC')
    window    = WindowModel.getallActiveRecords()
	
    if request.method == "POST":
        details = request.form
        data = DataSheetModel.updateRecord(details)
        if data == 1:
            flash('Record Updated successfully.')
            return redirect(url_for('datasheet.showlist', ID = testcaseID))
        else:
            info = DataSheetModel.getRecordByID(ID)
            flash('ERROR!  unable to update the Record, please try later')
            return render_template("datasheet/edit.html", **locals())
    info = DataSheetModel.getRecordByID(ID)
    return render_template("datasheet/edit.html", **locals());

@datasheet.route('/add/<ID>', defaults={'ID': None})
@datasheet.route("/add/<ID>", methods=['GET', 'POST'])
def add(ID):
    if ID != 'None':
        testcaseID = int(ID)
    else:
        testcaseID = ID
    browse = BrowseModel.getallActiveRecords()
    keywords   = KeywordsModel.getallActiveRecords()
    objectrepo     = ObjectRepoModel.getallActivetcmRecords()
    testdata     = TestDataModel.getallActiveRecords()
    testcasemaster    = TestCaseMasterModel.getallActivetcmRecords('tcm.ID ASC')
    window    = WindowModel.getallActiveRecords()
    if request.method == "POST":
        details = request.form
        print(details)
        #print(DataSheetModel)
        data = DataSheetModel.addRecord(details)
        if data == 1:
            flash('Record Created successfully.')
            return redirect(url_for('datasheet.showlist', ID = details['TestCaseID']))
        else:
            info = {'TestCaseID':details['TestCaseID'], 'Window': details['Window'], 'DF_type': details['DF_type'], 'object_repo_id': details['object_repo_id'], 'DataFields': details['DataFields'], 'DV_type': details['DV_type'], 'test_data_id': details['test_data_id'], 'DataValues': details['DataValues'], 'KeywordID': details['KeywordID'], 'StepCondition': details['StepCondition'], 'BrowserID': details['BrowserID'], 'FieldName': details['FieldName'], 'Comments': details['Comments']}
            flash('ERROR!  unable to add the Record, please try later')
            return render_template("datasheet/add.html", **locals());
    info = {'name': ''}
    return render_template("datasheet/add.html", **locals());
	
@datasheet.route('/delete/<ID>/<testcaseID>"', defaults={'testcaseID': None})
@datasheet.route("/delete/<ID>/<testcaseID>", methods=['GET'])
def delete(ID, testcaseID):
	data = DataSheetModel.deleteRecord(ID)
	if data == 1:
		flash('Record Deleted successfully.')
	else:
		flash('ERROR!  unable to update the Record, please try later')
	return redirect(url_for('datasheet.showlist', ID = testcaseID))

@datasheet.route('/changeRowOrder', methods=['POST'])
def changeRowOrder():
    data = request.json
    if(DataSheetModel.updateTestStepOnSorting(data)):
        return json.dumps({'success': True})
    else:
        return json.dumps({'success':False})