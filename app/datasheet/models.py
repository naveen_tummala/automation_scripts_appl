# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
import sys
from sqlalchemy import text, Column, String, Integer
from app.Base import Base
from flask import session
from pprint import pprint

# Define a User model
class DataSheetModel(Base):

    __tablename__ = 'data_sheet'

    # User Name
    TestCaseID       = db.Column(db.Integer, nullable = False)
    TestStep       = db.Column(db.Integer, nullable = False)
    Window    = db.Column(db.Integer,  nullable = False)
    object_repo_id    = db.Column(db.Integer,  nullable = True)
    DataFields    = db.Column(db.String(1000),  nullable = True)
    test_data_id = db.Column(db.Integer,  nullable = True)
    DataValues    = db.Column(db.String(1000),  nullable = True)
    KeywordID       = db.Column(db.Integer, nullable = False)
    StepCondition = db.Column(db.String(1000),  nullable = True)
    BrowserID = db.Column(db.Integer, nullable = False)
    FieldName = db.Column(db.String(1000),  nullable = True)
    Comments = 	db.Column(db.String(1000),  nullable = True)
    modified_by = db.Column(db.Integer,  nullable = True)
    created_by = db.Column(db.Integer,  nullable = True)    
    def __init__(self, TestCaseID):
         self.TestCaseID     = TestCaseID
    def __str__(self):
        return str(self.TestCaseID)
    
    def __repr__(self):
        return self #'<User %r>' % (self.name)
	
    @classmethod
    def getallActivetcmRecords(self, TestCaseID):
        condition = ''
        if TestCaseID != 'None':
            condition = " AND ds.TestCaseID = " + str(TestCaseID )
        sql = text("""SELECT 
                    ds.*,
                    tcm.Title as TestCase,
                    w.Name as WindowName,
					ors.Name as ObjectRepo,
					ots.Name as ObjectType,
					td.Name as TestData,
					td.ParamIndex as pindex,
					kw.Name as Keyword,
					br.Name as Browser				
                    FROM data_sheet ds
                    INNER JOIN test_case_master tcm ON tcm.ID = ds.TestCaseID
                    INNER JOIN windownames w ON w.id = ds.Window
					LEFT JOIN object_repo ors ON ors.id = ds.object_repo_id
					LEFT JOIN object_types ots ON ots.id = ors.Type
					LEFT JOIN test_data td ON td.id = ds.test_data_id
					LEFT JOIN keywords kw ON kw.id = ds.KeywordID
					LEFT JOIN browse br ON br.id = ds.BrowserID
					WHERE ds.Active = 1
					""" + condition + """
                    ORDER BY ds. TestCaseID, ds.TestStep ASC limit 0, 100 """)
        return db.session.execute(sql)
    
    @classmethod
    def getMaxStepIDofTestcase(self, TestCaseID):
        sql = text("""SELECT 
                     case when max(TestStep)  is null  then 0  else max(TestStep) end as stepno			
                    FROM data_sheet ds
                    WHERE ds.TestCaseID = """ + str(TestCaseID))
        max =  db.session.execute(sql)
        for i in max:
	        return i.stepno
	
    @classmethod
    def updateRecord(self, Info):
        try:
            dataSheet = self.query.filter_by(id=Info['id']).first()
            #dataSheet.TestCaseID = Info['TestCaseID'];
            #dataSheet.TestStep = 26;
            dataSheet.Window = Info['Window'];
            dataSheet.object_repo_id = Info['object_repo_id'];
            dataSheet.DataFields = Info['DataFields'];
            dataSheet.test_data_id = Info['test_data_id']
            dataSheet.DataValues = Info['DataValues'];
            dataSheet.KeywordID = Info['KeywordID'];
            dataSheet.StepCondition = Info['StepCondition'];
            dataSheet.BrowserID = Info['BrowserID'];
            dataSheet.FieldName = Info['FieldName'];
            dataSheet.Comments = Info['Comments'];
            dataSheet.modified_by = session['user_id']
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return 0
    @classmethod
    def addRecord(self, Info):
        try:
            testStep = DataSheetModel.getMaxStepIDofTestcase(Info['TestCaseID'])
            testStep = testStep + 1
            dataSheet = self(TestCaseID = Info['TestCaseID'])
            dataSheet.TestCaseID = Info['TestCaseID'];
            dataSheet.TestStep = testStep;
            dataSheet.Window = Info['Window'];
            dataSheet.object_repo_id = Info['object_repo_id'];
            dataSheet.DataFields = Info['DataFields'];
            dataSheet.test_data_id = Info['test_data_id']
            dataSheet.DataValues = Info['DataValues'];
            dataSheet.KeywordID = Info['KeywordID'];
            dataSheet.StepCondition = Info['StepCondition'];
            dataSheet.BrowserID = Info['BrowserID'];
            dataSheet.FieldName = Info['FieldName'];
            dataSheet.Comments = Info['Comments'];
            dataSheet.created_by = session['user_id']
            dataSheet.modified_by = ''
            db.session.add(dataSheet)
            db.session.commit()
            return 1
        except:
            print("Unexpected error:", sys.exc_info())
            return 0
    @classmethod
    def updateTestStepOnSorting(self ,Info):
        try:
            for rec in Info:
                row = self.query.filter_by(id=rec['rowID']).first()
                row.TestStep = rec['newSortedRowID'];
                db.session.commit()
            return 1
        except:
            print("Row sorting failed")
            return 0