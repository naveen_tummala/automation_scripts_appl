-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 09:52 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automation_scripts_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `browse`
--

CREATE TABLE `browse` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `browse`
--

INSERT INTO `browse` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'common', '2019-09-13 16:43:10', '0000-00-00 00:00:00', 9, 0, 1),
(2, 'skip', '2019-09-13 16:43:10', '0000-00-00 00:00:00', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'CLEARSESSION', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(2, 'ENTER', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(3, 'CLICK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(4, 'PAGEREFRESH', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(5, 'SPECIFICWAIT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(6, 'ENTERUNIQUETEXT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(7, 'WAITTIME', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(8, 'INPUTSUGGESTIONSELECTION', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(9, 'VERIFYTEXT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(10, 'ISDISPLAYED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(11, 'SELECT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(12, 'CLEAR', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(13, 'SCROLLDOWN', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(14, 'DRAGANDDROPFUNC', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(15, 'SWITCHTOFRAME', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(16, 'SELECTDEFAULTFRAME', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(17, 'Selectwindow', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(18, 'closewindow', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(19, 'CHECK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(20, 'COMPARETEXT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(21, 'HOVERANDCLICK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(22, 'ENTEREMAILID', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(23, 'IMPWAIT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(24, 'ISENABLED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(25, 'isdisabled', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(26, 'UNCHECK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(27, 'GOBACK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(28, 'JCLICK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(29, 'ENTERSTOREDVALUE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(30, 'OPENURL', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(31, 'ALERTACCEPT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(32, 'STORENUMBER', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(33, 'STORETEXT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(34, 'RANDOMHOURS', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(35, 'RANDOMMINTUES', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(36, 'SWITCHWINDOW', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(37, 'CLOSESCREEN', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(38, 'SCHEDULE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(39, 'WAITFORELEMENT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(40, 'clearenter', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(41, 'CURRENTDATECHECK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(42, 'VERIFYALERTTEXT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(43, 'VERIFY', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(44, 'ISNOTDISPLAYED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(45, 'ENTERSTOREDEMAIL', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(46, 'COPYANDPASTE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(47, 'HOVER', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(48, 'VERIFYELEMENTNOTEXIST', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(49, 'WAITI', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(50, 'STOREVALUE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(51, 'waititme', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(52, 'ISSELECTED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(53, 'IMPWAIT2', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(54, 'IMPWAIT5', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(55, 'IMPWAIT3', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(56, 'SPECIFICCLICK', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(57, 'DRDWDEFAULTVALUE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(58, 'verfiytext', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(59, 'MOUSEOVER', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(60, 'STORECONSULTATIONDETAILS', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(61, 'CLIENTSELECTIONINVOICEPAGE', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(62, 'ISNOTDISPALYED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(63, 'isdisplayed ', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(64, 'STOREREQCONSULTATIONDETAILS', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(65, 'VERIFYOBJECT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(66, 'IF:77:78', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(67, 'ACCEPT', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(68, 'IF:100:101', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(69, 'ISDISPALYED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(70, 'GETID', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(71, 'ALERTDISMISS', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(72, 'ISNOTSELECTED', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(73, '450', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(74, 'Selected', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(75, '7000', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(76, 'ISNOTDISPLAYED3', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(77, 'IDCREATOR', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(78, 'ISNOTDISPLYEAD', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1),
(79, 'skip', '2019-09-13 16:49:41', '0000-00-00 00:00:00', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created_date` datetime DEFAULT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(2, 'Admin', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(3, 'Advisor', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(4, 'Advisor Portal', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(5, 'Bill', '2019-09-20 15:06:13', '2019-09-20 09:36:13', 9, 0, 1),
(6, 'Case Code', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(7, 'Clients', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(8, 'Client org', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(9, 'ClientOrg/Request', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(10, 'Companies', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(11, 'Compliance', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(12, 'Contracts', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(13, 'Create lead', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(14, 'Double Blind', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(15, 'E2E', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(16, 'Emails', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(17, 'Employees', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(18, 'Events', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(19, 'Event Invoices', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(20, 'Event/Client Portal', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(22, 'Invoices - Epay', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(23, 'Invoices - Epay - Advisor Referral Invoices', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(24, 'Lead', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(25, 'Organisation', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(26, 'other', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(27, 'Public Debt', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(28, 'QuickPoll Invoices', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(29, 'Rate Cap', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(30, 'Request', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(31, 'Scheduling/Non Scheduling Workflows', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(32, 'Search 2.0', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(33, 'Set Permissions', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(34, 'Surveys', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(35, 'Time Zones', '2019-09-13 16:49:58', '0000-00-00 00:00:00', 9, 0, 1),
(36, 'Invoices', '2019-09-20 15:50:45', '2019-09-20 10:20:45', 0, 0, 1),
(37, 'Adhoc Invoices', '2019-09-20 15:59:22', '2019-09-20 10:29:22', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `object_repo`
--

CREATE TABLE `object_repo` (
  `ID` int(11) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Locator` varchar(200) NOT NULL,
  `Comments` varchar(500) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `object_repo`
--

INSERT INTO `object_repo` (`ID`, `Type`, `Name`, `Locator`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, '1', 'LoginEmail', '//*[@id=\'loginId\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(2, '1', 'LoginPassword', '//*[@id=\'loginPassword\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(3, '2', 'LoginButton', '//*[@class=\'yellowBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(4, '3', 'Compliance', '//*[@id=\'menu-bar\']//a[text()=\'Compliance\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(5, '3', 'CreateDncEntry', '//*[@id=\'subnav\']//*[text()=\'Create DNC Entry\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(6, '', 'CompanyNameMapped', '//input[@id=\'CompanyNameMapped\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(7, '', 'DNCPopup', '//*[@name=\'Popup\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(8, '', 'Createbutton', '//input[@value=\'create\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(9, '', 'Invoices', '//*[@id=\'menu-bar\']//a[text()=\'Invoices\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(10, '', 'CreateInvoice', '//*[@id=\'subnav\']//a[text()=\'Create Invoice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(11, '', 'AdvisorId', '//input[@id=\'advisor_id\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(12, '', 'CreateRequest', '//input[@id=\'createRequest\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(13, '', 'SubmitCreateInvoice', '//input[@name=\'submitCreateInvoice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(14, '4', 'Client', '//*[@name=\'clientID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(15, '', 'MeetingDate', '//*[@name=\'meetingDate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(16, '', 'MeetingHour', '//*[@id=\'meetingHour\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(17, '', 'MeetingMinute', '//*[@id=\'meetingMinute\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(18, '', 'MeetingDuration', '//*[@id=\'meetingDuration\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(19, '', 'PaymentMethodTypeID', '//*[@id=\'PaymentMethodTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(20, '', 'SSN', '//input[@id=\'SSN\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(21, '', 'CorporationName', '//*[@id=\'CorporationName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(22, '', 'ISOwnCorp', '//*[@id=\'ISOwnCorp_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(23, '', 'Street', '//*[@id=\'street\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(24, '', 'State', '//*[@id=\'State\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(25, '', 'CalculateButton', '//input[@id=\'calculateButton\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(26, '', 'Requests', '//*[@id=\'menu-bar\']//a[text()=\'Requests\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(27, '', 'CreateRequestLink', '//*[@id=\'subnav\']//a[text()=\'Create Request\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(28, '', 'ExternalName', '//input[@id=\'ExternalName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(29, '', 'ClientAutoComplete', '//input[@id=\'ClientAutoComplete\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(30, '', 'RequestTypeID', '//*[@id=\'RequestTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(31, '', 'TargetPopulationSpecialty1', '//*[@id=\'targetPopulationSpecialty_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(32, '', 'TargetPopulationSpecialty2', '//*[@id=\'targetPopulationSpecialty_2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(33, '', 'InpMaxResponses', '//input[@id=\'inpMaxResponses\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(34, '', 'InpPrice', '//input[@id=\'inpPrice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(35, '', 'USRadiobutton', '//input[@value=\'US\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(36, '', 'CilentRequestDetails', '//*[@id=\'requestNote\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(37, '', 'Sector', '//select[@id=\'aipd_1_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(38, '', 'Industry', '//select[@id=\'aipd_2_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(39, '', 'CreateRequestBtn', '//input[@id=\'createRequestBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(40, '', 'ScreeningQuestions', '//a[@id=\'PollLink\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(41, '', 'Questionsbutton', '//*[@id=\'tinymce\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(42, '', 'DoneEditing', '(//*[@class=\'submitButton\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(43, '', 'DoneEditing_Nextbutton', '(//*[@value=\'Save Question\'])[3]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(44, '', 'SaveQuestionDown', '(//*[@src=\'/core/images/b_saveQuestions.gif\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(45, '', 'SaveQuestionUpper', '(//*[@src=\'/core/images/b_saveQuestions.gif\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(46, '', 'Email', '//*[@id=\'EMail\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(47, '', 'AdvisorApplicationSubmit', '//input[@id=\'advisorApplicationSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(48, '', 'YesRadiobutton', '//*[@value=\'Yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(49, '', 'PollSubmit', '//input[@id=\'inpSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(50, '', 'MenuClients', '//*[@id=\'menu-bar\']//a[text()=\'Clients\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(51, '', 'CreateOrganization', '//*[@id=\'subnav\']//a[text()=\'Create Organization\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(52, '', 'YesButton', '//input[@id=\'YesBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(53, '', 'ClientOrganizationStatusID', '//*[@id=\'ClientOrganizationStatusID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(54, '', 'ClientOrganizationTypeID', '//select[@id=\'ClientOrganizationTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(55, '', 'ClientOrganizationSector', '//*[@id=\'checkBoxPre_atpd_1_AOE1_ms_1533134\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(56, '', 'ContractTypeID', '//select[@id=\'ContractTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(57, '', 'DataSourceID', '//*[@id=\'dataSourceID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(58, '', 'CoSubmit', '//*[@id=\'coSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(59, '', 'MenuRequestsLink', '//*[@id=\'menu-bar\']//*[text()=\'Requests\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(60, '', 'UseSchedulingWorkflow', '//*[@id=\'UseSchedulingWorkflow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(61, '', 'ConferenceBridgeNumbersBtn', '//*[@id=\'ConferenceBridgeNumbersBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(62, '', 'Name', '//input[@id=\'Name\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(63, '', 'RequestList', '//*[@id=\'subnav\']//a[text()=\'Request List\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(64, '', 'MyRadioButton', '//*[@id=\'fset1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(65, '', 'MyVerticalButton', '//*[@id=\'fset4\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(66, '', 'PMRadioButton', '//*[@id=\'fset3\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(67, '', 'CurrentEmployess', '//*[@id=\'title_pm\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(68, '', 'Okbutton', '//*[@id=\'pmmultiselectOK\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(69, '', 'AllOrgType', '(//span[@id=\'orgmultiselectspan\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(70, '', 'SLBAllOrgType', '//*[@id=\'orgTypeDD\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(71, '', 'AllOrgTypeOkButton', '//*[@id=\'orgmultiselectOK\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(72, '', 'AllRadiButton', '(//*[@id=\'fset2\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(73, '', 'SubMenuRequestSearch', '//*[@id=\'subnav\']//*[text()=\'Request Search\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(74, '', 'Lname', '//*[@id=\'lname\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(75, '', 'HedgeFund', '//*[@id=\'hf_chk\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(76, '', 'SubmitSearch', '//*[@id=\'submitSearch\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(77, '', 'RequestID', '//*[@id=\'requestID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(78, '', 'AdvisorsMenu', '//*[@id=\'menu-bar\']//*[text()=\'Advisors\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(79, '', 'CreateAdvisorsSubMenu', '//*[@id=\'subnav\']//*[text()=\'Create Advisor\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(80, '', 'SubmitSearchButton', '//*[@class=\'submitSearch\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(81, '', 'Rate', '//*[@id=\'Rate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(82, '', 'CurrencyID', '//*[@id=\'CurrencyID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(83, '', 'PhysicalAddressState', '//*[@id=\'PhysicalAddress_State\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(84, '', 'PhysicalAddressCountry', '//*[@id=\'PhysicalAddress_Country\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(85, '', 'PhysicalAddressTimeZoneID', '//*[@id=\'PhysicalAddress_TimeZoneID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(86, '', 'PhoneNumber', '//*[@id=\'Phone1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(87, '', 'RecrutingResourceID', '//*[@id=\'RecrutingResourceID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(88, '', 'Biography', '//*[@id=\'ShortBio\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(89, '', 'Employer', '//*[@id=\'Company_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(90, '', 'JobTitle', '//*[@id=\'JobFunction_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(91, '', 'StartMonth', '//*[@id=\'StartMonth_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(92, '', 'StartYear', '//*[@id=\'StartYear_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(93, '', 'SaveButton', '//*[@id=\'saveButton\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(94, '', 'ApplicationTabID', '(//*[contains(@id, \'ApplicationTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(95, '', 'RelatedTabID', '(//*[contains(@id, \'RelatedTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(96, '', 'AttachToProject', '//*[text()=\'Attach to Project\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(97, '', 'AttachAdvisorsRequest', '//select[contains(@id,\'inpPMEventSelect_\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(98, '', 'AddInpEventSubmit', '//*[@name=\'inpEventSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(99, '', 'ComplianceTabID', '(//*[contains(@id, \'ComplianceTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(100, '', 'ComplianceMenu', '//*[@id=\'menu-bar\']//*[text()=\'Compliance\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(101, '', 'ComplianceConflictAdvisorList', '//*[@id=\'subnav\']//*[text()=\'Compliance Conflict Advisor List\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(102, '', 'ComplianceList', '(//*[@id=\'advisorSearch\']//td[4]//a)[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(103, '', 'EditButtonBoardCertifications', '//*[@id=\'editButton_BoardCertifications_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(104, '', 'BoardCertifications', '//*[@id=\'BoardCertifications_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(105, '', 'SaveButtonBoardCertifications', '//*[@id=\'saveButton_BoardCertifications_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(106, '', 'BoardCertificationsSelection', '//*[@id=\'BoardCertifications_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(107, '', 'BoardCertificationsSelectionTwo', '//*[@id=\'BoardCertifications_1_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(108, '', 'SunshineActViewEditButton', '//*[@id=\'sunshineActViewEditButton_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(109, '', 'LicensureLastName', '//*[@id=\'lastName_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(110, '', 'LicensedHealthcareCheckBox', '//*[@id=\'isNotUSLic_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(111, '', 'PrfLastName', '//*[@id=\'lastName_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(112, '', 'PrfFirstName', '//*[@id=\'firstName_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(113, '', 'SaveButtonSunshineAct', '(//*[@id=\'saveButton_SunshineAct\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(114, '', 'EditButtonDrugsANDTechnology', '//*[@id=\'editButton_DrugsANDTechnology_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(115, '', 'DrugsANDTechnology', '//*[@id=\'DrugsANDTechnology_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(116, '', 'EditButtonResearchInterests', '//*[@id=\'editButton_ResearchInterests_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(117, '', 'ResearchInterests', '//*[@id=\'ResearchInterests_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(118, '', 'SaveButtonResearchInterests', '//*[@id=\'saveButton_ResearchInterests_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(119, '', 'EditButtonCompanyExperience', '//*[@id=\'editButton_CompanyExperience_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(120, '', 'CompanyExperience', '//*[@id=\'CompanyExperience_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(121, '', 'SaveButtonCompanyExperience', '//*[@id=\'saveButton_CompanyExperience_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(122, '', 'EditButtonProductExperience', '//*[@id=\'editButton_ProductExperience_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(123, '', 'ProductExperience', '//*[@id=\'ProductExperience_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(124, '', 'SaveButtonProductExperience', '//*[@id=\'saveButton_ProductExperience_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(125, '', 'TCHistoryTabID', '(//*[contains(@id, \'TCHistoryTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(126, '', 'ChangelogTabID', '(//*[contains(@id, \'ChangelogTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(127, '', 'ScheduleSlot', '(//*[contains(@id, \'slot\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(128, '', 'SaveSelectedAvailability', '//*[@value=\'Save Selected Availability\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(129, '', 'EventsTabID', '(//*[contains(@id, \'EventsTabID_\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(130, '', 'EventsListingTableLst', '//*[@id=\'eventsListingTable\']//*//td[5]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(131, '', 'EventsListingTableListTwo', '//*[@id=\'eventsListingTable\']//*//td[6]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(132, '', 'EditButtonDetailHeader', '//*[@id=\'editButton_Detail_Header\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(133, '', 'MinimumMinutes', '//*[@id=\'MinimumMinutes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(134, '', 'SaveButtonDetailHeader', '//*[@id=\'saveButton_Detail_Header\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(135, '', 'StampInternalUpdatesBtn', '//*[@id=\'stampInternalUpdatesBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(136, '', 'PhoneChk', '//*[@id=\'phoneChk\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(137, '', 'EmailChk', '//*[@id=\'emailChk\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(138, '', 'UpdatesEmailBtn', '//*[@id=\'updatesEmailBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(139, '', 'SendEmailBtn', '//*[@id=\'sendEmailBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(140, '', 'DisplayViewIDDetailHeader', '//*[@id=\'displayViewID_Detail_Header\']/*[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(141, '', 'SubmitInvoiceTxt', '//*[text()=\'Submit Invoice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(142, '', 'AccountOwnershipTypeID', '//*[@id=\'AccountOwnershipTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(143, '', 'BankAccountTypeID', '//select[@id=\'BankAccountTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(144, '', 'RoutingNumber', '//*[@id=\'RoutingNumber\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(145, '', 'AccountNumber', '//*[@id=\'AccountNumber\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(146, '', 'AmountHidden', '//*[@id=\'amountHidden\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(147, '', 'CalculateButton', '//*[@id=\'calculateButton\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(148, '', 'SubmitCreateInvoice', '//*[@id=\'submitCreateInvoice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(149, '', 'AttachedAdvisorOneofMYRequest', '//select[contains(@id,\'inpPMRequestSelect_\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(150, '', 'CreateLead', '//*[@id=\'subnav\']//*[text()=\'Create Lead\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(151, '', 'CCAsst', '//*[@id=\'CCAsst\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(152, '', 'AsstName', '//*[@id=\'AsstName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(153, '', 'AsstEmail', '//*[@id=\'AsstEmail\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(154, '', 'AsstPhone', '//*[@id=\'AsstPhone\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(155, '', 'PrefixID', '//*[@id=\'PrefixID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(156, '', 'StickyTitlebarSave', '//*[@id=\'sticky-titlebar\']//*[text()=\'Save\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(157, '', 'MenubarCompanies', '//*[@id=\'menu-bar\']//*[text()=\'Companies\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(158, '', 'ChangeAdvisorEmployer', '//*[@id=\'subnav\']//*[text()=\'Change Advisor Employer\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(159, '', 'keywordFrom', '//*[@id=\'keyword_from\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(160, '', 'KeywordNewEmployer', '//*[@id=\'keywordNewEmployer\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(161, '', 'LAdvisorDNDChk', '//*[text()=\'LAdvisorDND, FAdvisorDND\']/..//input', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(162, '', 'ReassignAdvisorsbutton', '(//*[@value=\'Reassign Advisors\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(163, '', 'CreateOrganization', '//*[@id=\'subnav\']//*[text()=\'Create Organization\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(164, '', 'MenubarClients', '//a[text()=\'Clients\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(165, '', 'YesButton', '//input[@id=\'YesBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(166, '', 'ClientOrganizationSectorforBusiness', '//*[@id=\'atpd_1_AOE1_ms\']//*[text()=\'Business Services\']/preceding-sibling::td', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(167, '', 'DataSourceIDGuide', '//select[@name=\'DataSourceID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(168, '', 'SaveImg', '//img[contains(@src,\'b_save\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(169, '', 'SubMenuCreateClient', '//*[@id=\'subnav\']//a[text()=\'Create Client\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(170, '', 'FirstName', '//*[@id=\'FirstName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(171, '', 'LastName', '//*[@id=\'LastName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(172, '', 'Country', '//*[@id=\'Country\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(173, '', 'TimeZoneID', '//*[@id=\'TimeZoneID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(174, '', 'Title', '//input[@id=\'Title\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(175, '', 'SalesPersonPMID', '//*[@id=\'SalesPersonPMID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(176, '', 'ClientStatusID', '//select[@id=\'ClientStatusID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(177, '', 'NewRequestForClient', '//*[@id=\'sticky-titlebar\']//a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(178, '', 'ClientOrganizationList', '//*[text()=\'Client Organization:\']/following-sibling::td/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(179, '', 'EditButtonClientOrganizationHeader', '//*[@id=\'editButton_ClientOrganization_Header\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(180, '', 'DisplayClientOrgConsultationCountsInCompliance', '//*[@id=\'DisplayClientOrgConsultationCountsInCompliance\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(181, '', 'ShowComplianceHistory', '//*[@id=\'showComplianceHistory\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(182, '', 'DonotDisplay', '//*[@id=\'DonotDisplay\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(183, '', 'UseComplianceWorkflow', '//*[@id=\'UseComplianceWorkflow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(184, '', 'InpComplianceSendClientSelectedAdvisors', '//*[@id=\'inpComplianceSendClientSelectedAdvisors\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(185, '', 'AllCompanyProtocol', '//*[@id=\'AllCompanyProtocol\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(186, '', 'EmployerConsentAllBanType', '//*[@id=\'EmployerConsentAllBanType_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(187, '', 'AllEmploymentThreshold', '//*[@id=\'AllEmploymentThreshold\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(188, '', 'SaveButtonClientOrganizationHeader', '//*[@id=\'saveButton_ClientOrganization_Header\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(189, '', 'ClassicAdvisorSearch', '//*[@id=\'ClassicAdvisorSearch\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(190, '', 'FindAdvisorsSerchButton', '(//*[@id=\'findAdvisorsSerchButton\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(191, '', 'PublicCheckbox', '(//*[@class=\'checkBox\']//input)[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(192, '', 'PrivateCheckbox', '(//*[@class=\'checkBox\']//input)[3]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(193, '', 'GevernmentCheckbox', '(//*[@class=\'checkBox\']//input)[4]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(194, '', 'AttachAdvisors', '(//*[@value=\'Attach Advisors\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(195, '', 'EditButtonClientCard', '//*[@id=\'editButton_ClientCard\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(196, '', 'IsComplianceOfficer', '//*[@id=\'IsComplianceOfficer\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(197, '', 'SaveButtonClientCard', '//*[@id=\'saveButton_ClientCard\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(198, '', 'PublicCompanyProtocol', '//*[@id=\'PublicCompanyProtocol\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(199, '', 'PrivateCompanyProtocol', '//*[@id=\'PrivateCompanyProtocol\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(200, '', 'GovernmentCompanyProtocol', '//*[@id=\'GovernmentCompanyProtocol\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(201, '', 'PublicConsultationMonth', '//*[@id=\'PublicConsultationMonth\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(202, '', 'PublicConsultation', '//*[@id=\'PublicConsultation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(203, '', 'PublicConsultationBanType', '//*[@id=\'publicConsultationBanType_2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(204, '', 'PrivateConsultationMonth', '//*[@id=\'PrivateConsultationMonth\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(205, '', 'PrivateConsultation', '//*[@id=\'PrivateConsultation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(206, '', 'PrivateConsultationBanType', '//*[@id=\'privateConsultationBanType_2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(207, '', 'GovernmentConsultationMonth', '//*[@id=\'GovernmentConsultationMonth\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(208, '', 'GovernmentConsultation', '//*[@id=\'GovernmentConsultation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(209, '', 'GovernmentConsultationBanType', '//*[@id=\'governmentConsultationBanType_2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(210, '', 'AdvEmailCheck', '(//*[@name=\'adv_email[]\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(211, '', 'HasQuickPollCompliance', '//*[@id=\'hasQuickPollCompliance\']/../img', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(212, '', 'CloseQuickPoll', '//*[@title=\'Close\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(213, '', 'ReplyQuestions', '(//*[text()=\'Reply\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(214, '', 'RespondQuestions', '(//*[text()=\'Respond\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(215, '', 'AdvReplyYes', '//*[@id=\'adv_reply_yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(216, '', 'PreferredPhoneNumber', '//*[@id=\'PreferredPhoneNumber\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(217, '', 'GPGTermsANDConditions', '//*[@id=\'GPG_TermsANDConditions\']/../span', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(218, '', 'GPGAdvisorTutorial', '//*[@id=\'GPG_AdvisorTutorial\']/../span', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(219, '', 'SendSubmit', '//*[@id=\'sendSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(220, '', 'EmailSendResponseBtn', '//*[@id=\'emailSendResponseBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(221, '', 'ClientPreApprovalEmail', '//*[@name=\'client_pre_approval_email[]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(222, '', 'ClientPreApprovedEmailComposer', '//*[@id=\'ClientPreApprovedEmailComposer\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(223, '', 'CheckboxcmpEmailUnsent', '//*[@class=\'checkbox cmpEmailUnsent\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(224, '', 'CompseEmailAdvisorID', '(//*[@id=\'compseEmailAdvisorID\'])[2]/img', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(225, '', 'AprrovedRadiobutton', '//*[text()=\'Approved\']/../input', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(226, '', 'CompOfficer', '(//*[starts-with(@id, \'compOfficer\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(227, '', 'Submit', '//*[@title=\'submit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(228, '', 'CheckboxCltEmailUnSent', '//*[@class=\'checkboxcltEmailUnSent\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(229, '', 'ClientEmailComposer', '//*[@id=\'ClientEmailComposer\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(230, '', 'ScheduleIconCell', '(//*[starts-with(@id,\'ScheduleIconCell\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(231, '', 'ScheduleSlotTimes', '(//*[starts-with(@id, \'slot\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(232, '', 'ScheduleButton', '//*[@value=\'Schedule\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(233, '', 'RequestOptions', '//*[@id=\'RequestOptions\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(234, '', 'CopyProjectwithAdvisorsOnly', '//*[text()=\'Copy Project with Advisors Only\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(235, '', 'AllAdvisors', '(//*[text()=\'All Advisors\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(236, '', 'CopyAdditionalSubMenuRequest', '//*[@id=\'copyAdditionalSubMenuRequest\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(237, '', 'RadioButtonNo', '//*[@value=\'No\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(238, '', 'Override', '//*[text()=\'Override\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(239, '', 'InpOverrideReason', '//*[contains(@id, \'inpOverrideReason_\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(240, '', 'ValueSubmit', '//*[@value=\'Submit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(241, '', 'OverrideCheckNote', '//*[contains(@id, \'overrideCheckNote\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(242, '', 'RemoveOverride', '//*[contains(@id, \'removeOverride\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(243, '', 'SubMenuAdvisorSearch', '//*[@id=\'subnav\']//*[text()=\'Advisor Search\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(244, '', 'TextJames', '//*[text()=\'Yeh, James\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(245, '', 'CilentName', '((//*[contains(@id, \'AdvisorRequestHistoryTable\')]//td[3])[1]//td)[1]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(246, '', 'OrgName', '((//*[contains(@id, \'AdvisorRequestHistoryTable\')]//td[3])[1]//td)[2]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(247, '', 'RequestName', '(//*[contains(@id, \'AdvisorHistoryRequest\')])[1]/td[2]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(248, '', 'MooDialogButtonNo', '//*[@id=\'MooDialog_button_No\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(249, '', 'RequestTypeIDName', '//select[@name=\'RequestTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(250, '', 'SubMenuBillClient', '//*[@id=\'subnav\']//a[text()=\'Bill Client\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(251, '', 'Service', '//*[@id=\'service\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(252, '', 'ConsultationUnits', '//*[@id=\'cu\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(253, '', 'ConsultationUnitsReason', '//*[@id=\'cu_adjustment\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(254, '', 'PicklistApproval', '//*[@id=\'subnav\']//a[text()=\'Picklist Approval\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(255, '', 'AddToPickList', '(//*[contains(@id,\'singleMatch\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(256, '', 'KeyWord', '//*[@id=\'keyword\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(257, '', 'Removefromthelist', '(//*[contains(@id,\'singleMatch2\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(258, '', 'PaidButton', '//*[@id=\'paidButton\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(259, '', 'MenuAccounting', '//*[@id=\'menu-bar\']//a[text()=\'Accounting\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(260, '', 'SubMenuExportToHSBC', '//*[@id=\'subnav\']//a[text()=\'Export To HSBC\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(261, '', 'StreetTwo', '//*[@id=\'street2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(262, '', 'RateCapConfirmationYes', '//*[@id=\'rate_cap_confirmation_yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(263, '', 'MobileText', '(//*[@type=\'text\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(264, '', 'MobileCheckBox', '(//*[@type=\'checkbox\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(265, '', 'RDB1020', '//*[@value=\'10-20\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(266, '', 'ComplianceYesRdb', '(//*[@value=\'Yes\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(267, '', 'RecordingConsentConfirmationYes', '//*[@id=\'recording_consent_confirmation_yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(268, '', 'CompAdvisorName', '(//*[contains(@id, \'advisorName_\')])[1]//a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(269, '', 'MenuEvents', '//*[@id=\'menu-bar\']//*[text()=\'Events\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(270, '', 'SubMenuCreateEvent', '//*[@id=\'subnav\']//*[text()=\'Create Event\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(271, '', 'InpTypeBreakfastLunch', '//*[@id=\'inpType_BreakfastLunch\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(272, '', 'SectorSelection', '//*[@id=\'aipd_1_inpIndustry1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(273, '', 'IndustrySector', 'SLB//*[@id=\'aipd_2_inpIndustry1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(274, '', 'InpStartDate', '//*[@name=\'EventDateTimeDay\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(275, '', 'InpEndDate', '//*[@name=\'EndDateTimeDay\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(276, '', 'EventSubmit', '//*[@type=\'submit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(277, '', 'EventAdvisorChecked', '(//*[contains(@id, \'advisorChecked\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(278, '', 'AttachAdvisorsOne', '(//*[@value=\'Attach Advisors\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(279, '', 'AdvAttRsvpEmail', '//*[@name=\'adv_att_rsvp_email[]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(280, '', 'HComposeAdvrsvpEmail', '(//*[contains(@src, \'h_composeadvrsvpemail.gif\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(281, '', 'AdvAttConfirmEmail', '//*[@name=\'adv_att_confirm_email[]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(282, '', 'HconfirmAgenda', '(//*[contains(@src, \'h_confirmagenda.gif\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(283, '', 'Filtercotype', '//*[@id=\'filter_cotype\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(284, '', 'EventBusinessServicesChk', '(//*[text()=\'Business Services\'])[3]/..//input', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(285, '', 'EventSearch', '//*[@value=\'Search\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(286, '', 'Hcomposeclientprersvpemail', '(//*[contains(@src, \'h_composeclientprersvpemail.gif\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(287, '', 'SubMenuCreateContract', '//*[@id=\'subnav\']//a[text()=\'Create Contract\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(288, '', 'StartDate', '//input[@id=\'startDate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(289, '', 'EndDate', '//input[@id=\'endDate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(290, '', 'OriginalEndDate', '//input[@id=\'originalEndDate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(291, '', 'ClientOrgInvoiceTypeID', '//select[@name=\'clientOrgInvoiceTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(292, '', 'RevenueItemID', '//select[@name=\'revenueItemID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(293, '', 'TermsContractValue', '//input[@name=\'termsContractValue\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(294, '', 'AnnualContractValue', '//input[@name=\'annualContractValue\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(295, '', 'CurrencyIDByName', '//select[@name=\'CurrencyID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(296, '', 'TermsValuePerRolledOverUnit', '//input[@name=\'termsValuePerRolledOverUnit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(297, '', 'TermsUnitsRolledOver', '//input[@name=\'termsUnitsRolledOver\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(298, '', 'TermsUnitOverageCharge', '//input[@name=\'termsUnitOverageCharge\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(299, '', 'IncentiveUnits', '//input[@name=\'incentiveUnits\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(300, '', 'TermsMaxUnits', '//input[@name=\'termsMaxUnits\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(301, '', 'Advisors', '//a[text()=\'Advisors\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(302, '', 'SubMenuCreateAdvisor', '(//a[text()=\'Create Advisor\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(303, '', 'ProceedwithAdvisorCreation', '//input[@value=\'Proceed with Advisor Creation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(304, '', 'FirstNameByName', '//input[@name=\'FirstName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(305, '', 'LastNameByName', '//input[@name=\'LastName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(306, '', 'RateByName', '//input[@name=\'Rate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(307, '', 'AddState', '//select[@name=\'AddState\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(308, '', 'AddCountry', '//select[@name=\'AddCountry\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(309, '', 'PhoneOne', '//input[@name=\'Phone1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(310, '', 'AttachedAOE', '//select[@name=\'atpd_1_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(311, '', 'YearsOfExperience', '//select[@id=\'YearsOfExperience\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(312, '', 'CompanyOne', '//*[@id=\'Company_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(313, '', 'JobFunctionOne', '//input[@id=\'JobFunction_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(314, '', 'StartMonthOne', '//select[@id=\'StartMonth_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(315, '', 'EndMonthOne', '//select[@id=\'EndMonth_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(316, '', 'EndYearOne', '//select[@id=\'EndYear_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(317, '', 'SaveButtonRequestCard', '//div[@id=\'saveButton_RequestCard\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(318, '', 'RequestsByText', '//a[text()=\'Requests\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(319, '', 'CalendarTwo', '(//a[text()=\'Calendar\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(320, '', 'TimezoneDD', '//select[@id=\'TimezoneDD\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(321, '', 'CalendarImage', '//img[contains(@src,\'/images/icons/calendar.png\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(322, '', 'Today', '//*[text()=\'Today\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(323, '', 'ElemTypeID', '//select[@id=\'elemTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(324, '', 'ElemTypeValue', '//input[@id=\'elemTypeValue\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(325, '', 'ClientListTwo', '(//a[text()=\'Client List\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(326, '', 'ClientName', '//input[@id=\'ClientName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(327, '', 'TrauilClient', '//a[text()=\'Trauil client\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(328, '', 'RequestFlagID', '//select[@name=\'RequestFlagID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(329, '', 'SelectReuest', '//select[@name=\'atpd_2_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(330, '', 'SelectClientOrganizationID', '//select[@name=\'clientOrganizationID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(331, '', 'SubMenuClientList', '//*[@id=\'subnav\']//a[text()=\'Client List\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(332, '', 'StartDateByStarts', '//*[starts-with(@id,\'StartDate\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(333, '', 'EndDateByStarts', '//*[starts-with(@id,\'EndDate\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(334, '', 'UseOnlyInternalIPAddresses', '//*[starts-with(@id,\'UseOnlyInternalIPAddresses\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(335, '', 'Title', '//*[@id=\'Title\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(336, '', 'Changelog', '//a[text()=\'Changelog\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(337, '', 'SubmitSearchByStarts', '//*[starts-with(@id,\'submitSearch\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(338, '', 'EditButtonContractCard', '//div[@id=\'editButton_Contract_Card\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(339, '', 'ChangelogLI', '//li[@id=\'changelog_LI\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(340, '', 'ExternalName', '//input[@id=\'ExternalName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(341, '', 'OkBtn', '//input[@id=\'okBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(342, '', 'CreateRequestBtn', '//input[@id=\'createRequestBtn\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(343, '', 'AdvEmailCheckbox', '(//*[starts-with(@id,\'adv_email_checkbox\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(344, '', 'ComposeEmailAdvisor', '//img[contains(@src,\'/images/h_composeEmailAdvisor.gif\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(345, '', 'PopupClose', '//span[@class=\'popupClose\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(346, '', 'AdvisorCheckedByStart', '//*[starts-with(@id,\'advisorChecked\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(347, '', 'TextFieldByClass', '//input[@class=\'textField\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(348, '', 'YesByValue', '(//input[@value=\'Yes\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(349, '', 'DayHeaderClickable', '(//div[@class=\'dayHeader clickable\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(350, '', 'DayHeaderClickableTwo', '(//div[@class=\'dayHeader clickable\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(351, '', 'Checkboxone', '(//span[@class=\'checkbox\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(352, '', 'Checkboxtwo', '(//span[@class=\'checkbox\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(353, '', 'SendImage', '//img[contains(@src,\'/core/images/b_send.gif\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(354, '', 'HcomposeEmailClientImage', '//img[contains(@src,\'/images/h_composeEmailClient.gif\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(355, '', 'SlotByStartstwo', '(//*[starts-with(@id, \'slot\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(356, '', 'AdvisorEmailsLink', '//a[@id=\'AdvisorEmailsLink\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(357, '', 'Respond', '//a[text()=\'Respond\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(358, '', 'CustomTextBytext', '//a//abbr[text()=\'Custom Text\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(359, '', 'SelectAnOption', '(//a//span[text()=\'Select an Option\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(360, '', 'SelectAnOptionOne', '//a//span[text()=\'Select an Option\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(361, '', 'PreApproval Email', '//a[text()=\'To Client #1 (Pre-Approval Email)\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(362, '', 'AdvisorListByText', '//a[text()=\'To Client #2 (Advisor List)\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(363, '', 'ToCompliance', '//a[text()=\'To Compliance\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(364, '', 'SchedulingConfirmationLNK', '//a[text()=\'Scheduling Confirmation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(365, '', 'Rescheduling', '//a[text()=\'Rescheduling\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(366, '', 'SchedulingUpdateLnk', '//a[text()=\'Scheduling Update\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(367, '', 'BusinessApproverLnk', '//a[text()=\'Business Approver\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(368, '', 'BusinessReply', '//a[text()=\'Reply\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(369, '', 'AdvEmailByCheck', '//input[@name=\'adv_email[]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(370, '', 'RecordingConsentConfirmationYes', '//input[@id=\'recording_consent_confirmation_yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(371, '', 'HcomposeEmailClientImg', '(//img[contains(@src,\'/images/h_composeEmailClient.gif\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(372, '', 'HComposeEmailComplianceimg', '(//img[contains(@src,\'/images/h_composeEmailCompliance.gif\')])', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(373, '', 'AcceptRadio371958', '//input[@id=\'acceptRadio371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(374, '', 'AdvID371958', '//select[@id=\'compOfficer[AdvID_371958]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(375, '', 'BtnsubmitImage', '(//img[contains(@src,\'/core/images/btn_submit.gif\')])', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(376, '', 'Email371958', '//input[@id=\'client_email_371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(377, '', 'HComposeEmailClientimgTwo', '(//img[contains(@src,\'/images/h_composeEmailClient.gif\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(378, '', 'SlotByThree', '(//*[starts-with(@id, \'slot\')])[3]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(379, '', 'SlotByFour', '(//*[starts-with(@id, \'slot\')])[4]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(380, '', 'InpRescheduleBtn371958', '//input[@id=\"inpRescheduleBtn_371958\"]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(381, '', 'SchedulingOverridePurposeTypeID', '//select[@id=\'SchedulingOverridePurposeTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(382, '', 'ContactComplianceandOverride', '//button[text()=\'Contact Compliance and Override\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(383, '', 'InpSendUpdateMeetingBtn371958', '//input[@id=\'inpSendUpdateMeetingBtn_371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(384, '', 'InpUpdateBtn371958', '//input[@id=\'inpUpdateBtn_371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(385, '', 'OrganizationListByText', '(//a[text()=\'Organization List\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(386, '', 'FsetOne', '//input[@id=\'fset1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(387, '', 'FsetFour', '//input[@id=\'fset4\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(388, '', 'FsetTwo', '//input[@id=\'fset2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(389, '', 'ClientFeedback', '//a[text()=\'Client Feedback \']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(390, '', 'RequestsOne', '(//a[text()=\'Requests\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(391, '', 'RequestListTwo', '(//a[text()=\'Request List\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(392, '', 'Request SearchTwo', '(//a[text()=\'Request Search\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(393, '', 'OrderBySelect', '//select[@id=\'orderBy\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(394, '', 'PageSize', '//select[@id=\'pageSize\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1);
INSERT INTO `object_repo` (`ID`, `Type`, `Name`, `Locator`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(395, '', 'Q&AHist', '//a[text()=\'Q & A Hist.\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(396, '', 'ReressionTesting', '//a[text()=\'reression Testing 28-11-2018\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(397, '', 'ComplianceByOne', '(//a[text()=\'Compliance\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(398, '', 'CompanyNameId', '//input[@id=\'company_name_id\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(399, '', 'RecurrenceFrequencyID', '//select[@id=\'recurrenceFrequencyID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(400, '', 'PaymentTerms', '//input[@id=\'paymentTerms\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(401, '', 'Cbx371958', '//input[@id=\'cbx_371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(402, '', 'CopyAdvisorsByStatusSelect', '//select[@id=\'copyAdvisorsByStatus\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(403, '', 'MassUpdateQualifiedBySelect', '//select[@id=\'massUpdateQualified\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(404, '', 'LabelToRequestID', '(//label[contains(@id,\'labelToRequestID\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(405, '', 'AdvEmailCheckbox371958', '//input[@id=\'adv_email_checkbox_371958\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(406, '', 'ProceedwithClientCreation', '//*[@value=\'Proceed with Client Creation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(407, '', 'ClientOrganizationIDByID', '//*[@id=\'ClientOrganizationID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(408, '', 'MenuCollapseChevron', '//img[@id=\'menuCollapseChevron\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(409, '', 'EventsByText', '//a[text()=\'Events\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(410, '', 'LoginByText', '//a[text()=\'Login\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(411, '', 'CreateEmployeeMenu', '//*[@id=\'subnav\']//a[text()=\'Create Employee\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(412, '', 'ProceedwithProjectManagerCreation', '//input[@value=\'Proceed with Project Manager Creation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(413, '', 'SectorBYAipd3', '//select[@id=\'aipd_3_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(414, '', 'SectorBYAipd4', '//select[@id=\'aipd_4_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(415, '', 'SectorBYAipd5', '//select[@id=\'aipd_5_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(416, '', 'RequestsByTextOne', '//a[text()=\'Requests\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(417, '', 'SaveButtonContractCardByID', '//div[@id=\'saveButton_Contract_Card\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(418, '', 'ContractListByMenu', '//*[@id=\'subnav\']//a[text()=\'Contract List\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(419, '', 'OrganizationListByMenu', '//*[@id=\'subnav\']//a[text()=\'Organization List\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(420, '', 'PageSizeByName', '//select[@name=\'pageSize\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(421, '', 'ChangePeriodByName', '//select[@name=\'changePeriod\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(422, '', 'AdminByText', '//a[text()=\'Admin\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(423, '', 'SetPermissionsBySubMenu', '//*[@id=\'subnav\']//a[text()=\'Set Permissions\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(424, '', 'Logout', '//a[contains(text(),\'Logout\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(425, '', 'ReportsByTextR', '//a[text()=\'Reports\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(426, '', 'FsetTwoById', '//input[@id=\'fset2\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(427, '', 'EmployeesByText', '//a[text()=\'Employees\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(428, '', 'CopyProjectwithAdvisorsAndLeadsByText', '//*[text()=\'Copy Project with Advisors and Leads\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(429, '', 'BusinessServicesAOE', '//*[@name=\'atpd_1_AOE1\']//*[text()=\'Business Services\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(430, '', 'BusinessSupportServicesAOE', '//*[@name=\'atpd_2_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(431, '', 'SchedulingWorkflowByText', '//*[text()=\'Scheduling Workflow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(432, '', 'SaveByValue', '//*[@value=\'Save\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(433, '', 'FnameByID', '//*[@id=\'fname\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(434, '', 'BusinessSupportServicesAtpdAoe', '//*[@name=\'atpd_2_AOE1\']//*[text()=\'Business Support Services\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(435, '', 'RegularPhoneConsultationByRequestTypeID', '//*[@id=\'RequestTypeID\']//*[text()=\'Regular Phone Consultation\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(436, '', 'InvUnpaid', '//input[@id=\'inv_unpaid\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(437, '', 'GoToInvoiceByTitle', '(//*[contains(@title,\'Go to Invoice\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(438, '', 'Consult&Ad-HocByText', '(//a[text()=\'Consult. & Ad-Hoc\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(439, '', 'InvInprocess', '//input[@id=\'inv_inprocess\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(440, '', 'SubmitSearchByTittle', '//input[@title=\'Submit Search\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(441, '', 'InvPaid', '//input[@id=\'inv_paid\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(442, '', 'UnPaid', '//*[text()=\'Unpaid (\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(443, '', 'TitleBarButtonBtnPrimar', '//*[@class=\'titleBarButton btn btn-primary\' and text()=\'Save\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(444, '', 'Note', '//*[@id=\'Note\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(445, '', 'InpCopyRequestWLeadsBtnById', '//*[starts-with(@id,\'inpCopyRequestWLeadsBtn\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(446, '', 'OnlyAdvisorsAcceptedAndLeadsByText', '//*[text()=\'Only Advisors Accepted and Leads\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(447, '', 'DoubleBlinded', '//label[contains(text(),\'Double Blinded\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(448, '', 'CopyRequestByText', '//*[text()=\'Copy Request\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(449, '', 'AddThisAlias', '(//*[starts-with(@id,\'AddThisAlias\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(450, '', 'SimilarMatches', '//*[text()=\'Similar Matches \']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(451, '', 'UpdateCompaniesSubmit', '//*[@id=\'updateCompaniesSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(452, '', 'KeywordCompUsage', '//*[@id=\'keywordCompUsage\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(453, '', 'ConsultInvoices', '//*[@id=\'subnav\']//*[text()=\'Consult. Invoices\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(454, '', 'AdHocInvoices', '//*[@id=\'subnav\']//*[contains(text(),\'Ad-Hoc Invoices\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(455, '', 'InvRejected', '//input[@id=\'inv_rejected\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(456, '', 'SubMenuEventInvoices', '//*[@id=\'subnav\']//*[contains(text(),\'Event Invoices\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(457, '', 'ALL', '//a[starts-with(text(),\'All\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(458, '', 'SubMenuQuickPollInvoices', '//*[@id=\'subnav\']//*[contains(text(),\'Quick Poll Invoices\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(459, '', 'SubMenuConsultAdHoc', '//*[@id=\'subnav\']//*[contains(text(),\'Consult. & Ad-Hoc\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(460, '', 'Reset', '//*[@type=\'reset\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(461, '', 'FilterId', '//*[@id=\'filterId\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(462, '', 'ExcelImg', '//*[@src=\'/images/icons/excel.gif\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(463, '', 'MooDialogButtonYes', '//*[@id=\'MooDialog_button_Yes\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(464, '', 'EIN', '//*[@class=\'secNav yellowOnTab\']//*[contains(text(),\'EIN\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(465, '', 'AdvisorsByHref', '(//*[@class=\'nowrap lightcolumn flx\']//*[contains(@href,\'/advisors\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(466, '', 'TMT', '//*[@class=\'secNav yellowOnTab\']//*[contains(text(),\'TMT\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(467, '', 'AdvisorCheckedWithFive', '(//*[starts-with(@id,\'advisorChecked\')])[5]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(468, '', 'AdvisorCheckedWithSix', '(//*[starts-with(@id,\'advisorChecked\')])[6]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(469, '', 'CbxOne', '(//*[starts-with(@id,\'cbx\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(470, '', 'CbxTwo', '(//*[starts-with(@id,\'cbx\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(471, '', 'AdvEmailCheckboxOne', '(//*[starts-with(@id,\'adv_email_checkbox\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(472, '', 'AreplyOne', '(//*[starts-with(@id,\'areply\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(473, '', 'Accept', '//*[text()=\'Accept\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(474, '', 'Q736497choice', '//input[@name=\'q736497_2_choice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(475, '', 'Q759685Choice', '//input[@name=\'q759685_1_choice\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(476, '', 'AcceptRecordingConsent', '//input[@name=\'AcceptRecordingConsent\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(477, '', 'ResponseIDByName', '//input[@name=\'ResponseID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(478, '', 'PreferredPhoneNumberByTwo', '//select[@name=\'PreferredPhoneNumber\']/option[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(479, '', 'BackupPreferredPhoneNumber', '//select[@name=\'BackupPreferredPhoneNumber\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(480, '', 'Slot16', '(//*[starts-with(@id,\'slot_\')])[16]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(481, '', 'BackgroundPosition', '(//span[@style=\'background-position: 0px 0px;\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(482, '', 'GPGAdvisorTutorialLabel', '//label[@id=\'GPG_AdvisorTutorial_label\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(483, '', 'ComposeEmailComplianceByIMG', '//*[@src=\'/images/h_composeEmailCompliance.gif\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(484, '', 'AcceptRadio', '//input[starts-with(@id,\'acceptRadio\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(485, '', 'SelectComplianceOfficerByTwo', '(//select[@first_option=\'Select a Compliance Officer\'])/option[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(486, '', 'ScheduleIconByOne', '(//img[starts-with(@id,\'ScheduleIcon\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(487, '', 'Slot315', '(//*[starts-with(@id,\'slot_\')])[315]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(488, '', 'BtnSendEmailByIMG', '//*[@src=\'/images/btnSendEmail.gif\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(489, '', 'MenuSurveys', '//ul[@id=\'menu-bar\']//a[text()=\'Surveys\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(490, '', 'SubmitSearchFloatLeftMarginRightMini', '//div[@class=\'floatWrapper formSubmitButtons\']//*[@class=\'submitSearch floatLeft marginRightMini\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(491, '', 'AttachedAdvisorsByStartswith', '//a[starts-with(text(),\'Attached Advisors\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(492, '', 'FilterName', '//input[@id=\'filter_name\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(493, '', 'IconArrowDownBoxImg', '//span[@id=\'adv_att_filter_hs\']//img[@src=\'/images/icons/icon_arrow_down_box.gif\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(494, '', 'IconArrowDownBoxCLT', '//span[@id=\'clt_att_filter_hs\']//img[@src=\'/images/icons/icon_arrow_down_box.gif\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(495, '', 'SubnavLink', '//*[@id=\'subnav\']/li[5]/a', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(496, '', 'LunchByStarts', '//label[starts-with(text(),\'Breakfast / Lunch\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(497, '', 'OthersByStarts', '//label[starts-with(text(),\'Other\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(498, '', 'ALLByStarts', '//label[starts-with(text(),\'All\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(499, '', 'FilterEventAfter', '//input[@id=\'filter_eventAfter\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(500, '', 'Teleconference', '//label[starts-with(text(),\'Teleconference\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(501, '', 'AtpdInpIndustryOne', '//*[@name=\'atpd_1_inpIndustry1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(502, '', 'AtpdInpIndustryTwo', '//*[@name=\'atpd_2_inpIndustry1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(503, '', 'CCalendarIcon', '(//img[@class=\'cCalendarIcon\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(504, '', 'InpEventNameById', '//*[@id=\'inpEventName\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(505, '', 'ActiveByInput', '(//span[text()=\'Active\']/../preceding-sibling::td//input)[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(506, '', 'AttachedClientsAnchorById', '//*[@id=\'AttachedClients_anchor\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(507, '', 'ComposeclientprersvpemailBySrc', '(//*[@src=\'/images/h_composeclientprersvpemail.gif\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(508, '', 'PopupCloseByClass', '//*[@class=\'popupClose\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(509, '', 'ClntIntTitle', '//td[text()=\'Clnt/Int Title\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(510, '', 'InpCopyRequestBtnByStarts', '//*[starts-with(@id,\'inpCopyRequestBtn\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(511, '', 'Test8538', '//a[text()=\'test 8538\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(512, '', 'SaveByText', '//*[text()=\'Save\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(513, '', 'SubMenuTrackerInvoices', '//*[@id=\'subnav\']//*[contains(text(),\'Tracker Invoices\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(514, '', 'FilterPaymentStart', '//input[@id=\'filterPaymentStart\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(515, '', 'ConsultInvoicesBySubMenu', '(//*[@id=\'subnav\']//*[contains(text(),\'Consult. Invoices\')])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(516, '', 'IsRateCapByChk', '//*[@id=\'IsRateCap\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(517, '', 'IsClientRateCapByChk', '//*[@id=\'IsClientRateCap\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(518, '', 'NotifyPMOnAdvResponseByChk', '//*[@id=\'NotifyPMOnAdvResponse\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(519, '', 'IsDoubleBlindedByChk', '//*[@id=\'IsDoubleBlinded\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(520, '', 'EnableCallRecordingsByChk', '//*[@id=\'EnableCallRecordings\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(521, '', 'FlagPremiumAdvisorsByChk', '//*[@id=\'FlagPremiumAdvisors\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(522, '', 'DefaultLeadsAsExpressByChk', '//*[@id=\'DefaultLeadsAsExpress\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(523, '', 'AutomateThanksToAdvisor', '//*[@id=\'AutomateThanksToAdvisor\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(524, '', 'DateByone', '(//*[@id=\'date\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(525, '', 'SubmitSearchByIdone', '(//*[@id=\'submitSearch\'])[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(526, '', 'IndustryDefinitionListNEWarrow', '//*[@id=\'industryDefinitionList_NEW_arrow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(527, '', 'IndustryDefinitionListNEWarrowImage', '//img[contains(@src,\'/images/icons/arrow_close.gif\') and @id=\'industryDefinitionList_NEW_arrow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(528, '', 'IndustryTaxonomyInfo1MutliSelect', '//*[@id=\'industry_1_taxonomyInfo1_mutliSelect\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(529, '', 'SelectAllTaxonomyInfo1', '//*[@id=\'selectAll1_taxonomyInfo1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(530, '', 'AquisitionListArrow', '//*[@id=\'aquisitionList_arrow\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(531, '', 'Aquisition', '//*[@id=\'aquisition\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(532, '', 'AdvisorCheckedBy24', '(//*[starts-with(@id,\'advisorChecked\')])[24]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(533, '', 'AdvisorCheckedAndActiveByText', '(//*[contains(text(),\'Active\')])[1]/../preceding-sibling::td/input[starts-with(@id,\'advisorChecked\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(534, '', 'BiographyByName', '//textarea[@name=\'Biography\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(535, '', 'RadioByStarts', '(//*[starts-with(@class,\'radio\')])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(536, '', 'IfNoneCheckHere', '//*[contains(text(),\'If none, check here\')]/following-sibling::span', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(537, '', 'MarginBottomSmall', '(//*[@class=\'marginBottomSmall\']//following::span)[1]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(538, '', 'MarginBottomSmallTwo', '(//*[@class=\'marginBottomSmall\']//following::span)[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(539, '', 'DrugsANDTechnologyOne', '//*[@id =\'DrugsANDTechnology_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(540, '', 'ResearchInterestsOne', '//*[@id =\'ResearchInterests_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(541, '', 'BoardCertificationsOne', '//*[@id =\'BoardCertifications_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(542, '', 'CompanyExperienceOne', '//*[@id =\'CompanyExperience_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(543, '', 'ProductExperienceOne', '//*[@id =\'ProductExperience_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(544, '', 'ComplianceConflictsOne', '//*[@id =\'ComplianceConflicts_1_0\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(545, '', 'NewAdvisorSearchById', '//*[@id=\'NewAdvisorSearch\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(546, '', 'QueryID', '//*[@id=\'query\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(547, '', 'SearchSubmit', '//*[@class=\'searchSubmit\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(548, '', 'CVById', '//*[@id=\'CV\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(549, '', 'AdminBytextA', '//a[text()=\'Admin\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(550, '', 'EditClientsByText', '//*[contains(text(),\'Edit Clients\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(551, '', 'MoveById', '//*[@id=\'move\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(552, '', 'ResetById', '//*[@id=\'reset\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(553, '', 'LogOutByContians', '//*[contains(text(),\'Logout\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(554, '', 'GetPermissions', '//*[@id=\'getPermissions\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(555, '', 'RemoveById', '//*[@id=\'remove\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(556, '', 'InvoicesBytextLnk', '//*[text()=\'Invoices\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(557, '', 'ReassignClientOrgByContains', '//*[contains(text(),\'Reassign Client Org\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(558, '', 'ElemTypeValueById', '//*[@id=\'elemTypeValue\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(559, '', 'AdvisorCheckedBYPreceding', '(//*[contains(text(),\'T&C\')])[1]/../preceding-sibling::td/input[starts-with(@id,\'advisorChecked\')]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(560, '', 'ClientAttachedPrsvpEmail', '//*[@name=\'clt_att_prsvp_email[]\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(561, '', 'ClientPreApprovalEmail371958', '(//input[@id=\'client_pre_approval_email_371958\'])', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(562, '', 'ComplianceEmail371958', '(//input[@id=\'compliance_email_371958\'])', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(563, '', 'ScheduleByButtontext', '//button[text()=\'Schedule\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(564, '', 'btnSave', '//*[@id=\'saveButton_RequestCard\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(565, '', 'btnSendEmail', 'sendEmailBtn', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(566, '', 'Clientportal', '//a[@class=\'titleOption submitSearch clickable floatRight\'][text()=\'Login\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(567, '', 'slbprefix', '//*[@name = \'PrefixID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(568, '', 'slbclientorg', '//*[@id=\'ClientOrganizationID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(569, '', 'slbstate', '//*[@id=\'State\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(570, '', 'slbRequestType', '//*[@id=\'RequestTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(571, '', 'slbPhyTimezone', '//*[@id=\'PhysicalAddress_TimeZoneID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(572, '', 'slbPhoneType', '//*[@id=\'Phone1TypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(573, '', 'slbRecrutingResource', '//*[@id=\'RecrutingResourceID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(574, '', 'slbSector', '//*[@id=\'aipd_1_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(575, '', 'slbIndustry', '//*[@id=\'aipd_2_AOE1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(576, '', 'slbGLG', '//*[@id=\'profile_competitor_gerson\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(577, '', 'slbColeman', '//*[@id=\'profile_competitor_coleman\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(578, '', 'slbDeMatt', '//*[@id=\'profile_competitor_dmm\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(579, '', 'slbLee', '//*[@id=\'profile_competitor_ls\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(580, '', 'slbPrimary', '//*[@id=\'profile_competitor_pi\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(581, '', 'slbCogno', '//*[@id=\'profile_competitor_cognolink\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(582, '', 'slbRidge', '//*[@id=\'profile_competitor_ridgetop\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(583, '', 'slbAlpha', '//*[@id=\'profile_competitor_alphasights\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(584, '', 'slbAthe', '//*[@id=\'profile_competitor_atheneum\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(585, '', 'slbCap', '//*[@id=\'profile_competitor_capvision\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(586, '', 'slbAdve', '//*[@id=\'profile_competitor_adventus_bcc\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(587, '', 'slbOther', '//*[@id=\'profile_competitor_other\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(588, '', 'slbEmployee', '//*[@id=\'pmID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(589, '', 'slbGoto', '//*[@id=\'elemTypeID\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(590, '', 'slbperpage', '(//*[@id=\'per_page\'])[2]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(591, '', 'ClassicAdvisorEmail', '//*[@id=\'email\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(592, '', 'StateInvoice', '//*[@id=\'state\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(593, '', 'StartYearOne', '//select[@id=\'StartYear_1\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(594, '', 'EmpPassword', '//*[@id=\'Password\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(595, '', 'EmpCreatebutton', '//input[@value=\'Create\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(596, '', 'createEventLink', '//ul[@id=\'subnav\']//a[text()=\'Create Event\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(597, '', 'Emails', '//input[@id=\'Email\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(598, '', 'InpEndDate', '//*[@id=\'inpEndDate\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(599, '', 'Emailid', '//input[@id=\'email\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(600, '', 'ReturnedAgreement', '//td[text()=\'Returned Agreement\']', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1),
(601, '', 'slot415', '(//*[starts-with(@id,\'slot_\')])[415]', '', '2019-09-13 16:53:38', '0000-00-00 00:00:00', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `object_types`
--

CREATE TABLE `object_types` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `object_types`
--

INSERT INTO `object_types` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'TXTobj', '2019-09-13 16:53:51', '0000-00-00 00:00:00', 9, 0, 1),
(2, 'BTNobj', '2019-09-13 16:53:51', '0000-00-00 00:00:00', 9, 0, 1),
(3, 'XPHobj', '2019-09-13 16:53:51', '0000-00-00 00:00:00', 9, 0, 1),
(4, 'SLBobj', '2019-09-13 16:53:51', '0000-00-00 00:00:00', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` date NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`) VALUES
(1, 'Medium', '2019-09-13', '0000-00-00 00:00:00', 9, 0),
(2, 'Complex', '2019-09-13', '0000-00-00 00:00:00', 9, 0),
(3, 'High Complex', '2019-09-23', '0000-00-00 00:00:00', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_case_master`
--

CREATE TABLE `test_case_master` (
  `TCID` int(11) NOT NULL,
  `Title` varchar(500) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `TestRailCaseNo` varchar(20) NOT NULL,
  `PriorityID` int(11) NOT NULL,
  `assignedUserID` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `test_case_status`
--

CREATE TABLE `test_case_status` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_case_status`
--

INSERT INTO `test_case_status` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'Pending', '2019-09-23 15:12:11', '0000-00-00 00:00:00', 0, 0, 1),
(2, 'In Progress', '2019-09-23 15:12:11', '0000-00-00 00:00:00', 0, 0, 1),
(3, 'Completed', '2019-09-23 15:12:26', '0000-00-00 00:00:00', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_data`
--

CREATE TABLE `test_data` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Value` varchar(200) NOT NULL,
  `ParamIndex` int(3) NOT NULL,
  `Comments` varchar(500) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_data`
--

INSERT INTO `test_data` (`ID`, `Name`, `Value`, `ParamIndex`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'username', 'automationteam@guidepointglobal.com', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(2, 'password', 'a', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(3, 'password', 'sss', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(4, 'password', 'test@123', 3, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(5, 'Data_175', 'Value labs', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(6, 'Data_177', 'autoCConscheduleClient@gmail.com', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(7, 'Data_178', 'autoCConscheduleClient@gmail.com', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(8, 'Data_195', 'rupesh b - (N/A)', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(9, 'Data_196', 'rupesh.b@valuelabs.com', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(10, 'Data_196', '01251 Org', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(11, 'Data_187', '55296', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(12, 'Data_199', 'Comp client', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(13, 'Data_200', '31532', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(14, 'Data_201', '53163', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(15, 'Data_202', 'Comp client', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(16, 'Data_203', '55297', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(17, 'Data_197', '55298', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(18, 'Data_420', '47317', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(19, 'Data_421', '47317', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(20, 'Data_422', '47317', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(21, 'Data_423', '47317', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(22, 'Data_425', '47317', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(23, 'Data_431', '55443', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(24, 'Data_431', 'Auto1556725801881@automation.i n', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(25, 'Data_432', '31532', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(26, 'Data_434', '55444', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(27, 'Data_437', '55445', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(28, 'Data_438', '55446', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(29, 'Data_446', '55447', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(30, 'Data_447', 'AUTO1556711876677@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(31, 'Data_449', 'usha1556714160475@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(32, 'Data_435', 'Auto1556731979795@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(33, 'Data_435', 'Auto1556732165377@automation.in', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(34, 'Data_435', 'Auto1564139068082@automation.in', 3, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(35, 'Data_439', 'Auto1556731979795@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(36, 'Data_439', 'Auto1556732165377@automation.in', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(37, 'Data_466', 'peter.meyer@covidien.com', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(38, 'Data_467', ' AUTO1560248227249@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(39, 'Data_468', 'AUTO1560431307100@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(40, 'Data_469', 'AUTO1560521004041@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(41, 'Data_470', 'AUTO1560847640101@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(42, 'Data_471', 'AUTO1560852839907@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(43, 'Data_472', 'Auto129871923123@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(44, 'Data_472', '635389', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(45, 'Data_474', ' AUTO1560248227249@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(46, 'Data_499', 'AUTO1562502453759@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(47, 'Data_500', 'AUTO1562510252829@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(48, 'Data_500', 'Rate1', 2, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(49, 'Data_500', 'Cap1', 3, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(50, 'Data_502', 'AUTO1562670794077@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(51, 'Data_503', 'AUTO1562672129163@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1),
(52, 'Data_504', 'AUTO1562672129163@automation.in', 1, '', '2019-09-13 17:07:30', '0000-00-00 00:00:00', 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Name`, `username`, `password`, `role_id`, `created`, `active`) VALUES
(1, 'Haribabu', 'haribabu.chereddy@valuelabs.com', 'welcome', 6, '2019-09-16 11:29:08', 1),
(2, 'Anil', 'anil.komma@valuelabs.com', 'welcome@123', 7, '2019-09-13 11:18:04', 1),
(3, 'Navya', 'navya.kutcharlapati@valuelabs.com', 'welcome@123', 8, '2019-09-13 11:18:16', 1),
(4, 'Srilatha', 'srilatha.amberkar@valuelabs.com', 'welcome@123', 7, '2019-09-13 11:18:19', 1),
(5, 'Rupesh', 'rupesh.bramandlapalli@valuelabs.com', 'welcome@123', 8, '2019-09-13 11:18:22', 1),
(6, 'Srikanth', 'srikanth.thumukuntla@valuelabs.com', 'welcome@123', 2, '2019-09-13 11:18:26', 1),
(7, 'Santhosh', 'santhosh.gundapu@valuelabs.com', 'welcome@123', 8, '2019-09-13 11:18:29', 1),
(8, 'Tharun', 'tharun.jonnadula@valuelabs.com', 'welcome@123', 1, '2019-09-13 11:18:35', 1),
(9, 'Naveen', 'naveen.tummala@valuelabs.com', 'welcome@123', 1, '2019-09-17 14:16:04', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `browse`
--
ALTER TABLE `browse`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `object_repo`
--
ALTER TABLE `object_repo`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `object_types`
--
ALTER TABLE `object_types`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `test_case_master`
--
ALTER TABLE `test_case_master`
  ADD PRIMARY KEY (`TCID`),
  ADD UNIQUE KEY `TestRailCaseNo` (`TestRailCaseNo`),
  ADD UNIQUE KEY `unique_testcase_title` (`Title`);

--
-- Indexes for table `test_case_status`
--
ALTER TABLE `test_case_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `test_data`
--
ALTER TABLE `test_data`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `browse`
--
ALTER TABLE `browse`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `object_repo`
--
ALTER TABLE `object_repo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=602;

--
-- AUTO_INCREMENT for table `object_types`
--
ALTER TABLE `object_types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_case_master`
--
ALTER TABLE `test_case_master`
  MODIFY `TCID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test_case_status`
--
ALTER TABLE `test_case_status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_data`
--
ALTER TABLE `test_data`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
