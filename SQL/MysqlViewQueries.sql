-- View to retrieve data in datasheet format--
CREATE VIEW datasheet AS
SELECT 
dt.testCaseId,
tcm.Title as title,
dt.TestStep,
wn.Name as Window,
(CASE WHEN (ceil(dt.object_repo_id) != 0) THEN CONCAT(objt.Name,':',objr.Name) ELSE dt.DataFields END) as DataFields,
(CASE WHEN (ceil(dt.test_data_id) != 0) THEN CONCAT('dt:',td.Name,'#',td.ParamIndex) ELSE dt.DataValues END) as DataValues,
kw.Name as Action,
dt.StepCondition as Conditions,
br.Name as Browser,
dt.FieldName as FieldName,
dt.Comments as Comments,
'GPG' as AppName
FROM `data_sheet` as dt
INNER JOIN test_case_master tcm ON tcm.ID = dt.testCaseId
LEFT JOIN Keywords as kw ON kw.ID = dt.keywordID
LEFT JOIN Window as wn ON wn.ID = dt.Window
LEFT JOIN object_repo objr ON objr.ID = dt.object_repo_id
LEFT JOIN object_types objt ON objt.ID = objr.Type
LEFT JOIN test_data td ON td.ID = dt.test_data_id
LEFT JOIN browse br ON br.ID = dt.BrowserID
WHERE dt.Active = 1

--View to retrieve Test Data in TestData Sheet format
CREATE VIEW TestData AS
SELECT 
Name as ParameterName,
Value as ParameterValue,
ParamIndex as pIndex,
Comments as Comments,
'GPG' as AppName
FROM test_data
WHERE Active = 1

--View to retrieve Object Repo in ObjectRepo Sheet format
CREATE VIEW ObjectRepo AS
SELECT 
ot.Name as ObjectType,
obr.Name as ObjectName,
obr.Locator as ObjectLocator,
obr.Comments as Comments,
'GPG' as AppName
FROM object_repo obr
LEFT JOIN object_types ot ON ot.ID = obr.Type
WHERE obr.Active = 1

--View to retrieve test case master in sheet format
CREATE VIEW testcasemaster AS
SELECT
tcm.ID as TCID,
tcm.Title,
tcm.TestRailCaseNo,
p.Name as priority,
u.Name as owner,
'GPG' as AppName
FROM test_case_master as tcm
INNER JOIN priority p ON p.ID = tcm.PriorityID
INNER JOIN users u ON u.id = tcm.assignedUserID
WHERE tcm.Active = 1



-- Sample Queries may be useful
/*

SELECT count(tcm.Name), tcm.Requirement, m.ID as moduleID, m.Name as ModuleName  FROM `testcasemastertemp` tcm
INNER JOIN modules m ON m.Name = tcm.Module

INSERT INTO test_case_master (Title, ModuleID, TestRailCaseNo, PriorityID, assignedUserID, status, Created_date, Active)

SELECT tcm.Name, m.ID as moduleID, tcm.Requirement, p.ID as priorityID, u.ID as userID, 1, CURRENT_TIMESTAMP,1 FROM `testcasemastertemp` tcm
INNER JOIN modules m ON m.Name = tcm.Module
INNER JOIN priority p ON p.Name = tcm.priority
INNER JOIN users u on u.Name = tcm.Owner	
	
	
SELECT DISTINCT tcm.Name, m.ID as moduleID, m.Name as ModuleName, tcm.Requirement, p.ID as priorityID, p.Name as priorityName, u.Name as username  FROM `testcasemastertemp` tcm
INNER JOIN modules m ON m.Name = tcm.Module
INNER JOIN priority p ON p.Name = tcm.priority
INNER JOIN users u on u.Name = tcm.Owner

SELECT tcm.Name, tcm.Requirement, (SELECT Name FROM modules WHERE Name = tcm.Module) as ModuleName, (SELECT Name FROM priority WHERE Name = tcm.priority) as priorityName, (SELECT Name FROM users WHERE Name = tcm.owner) as username  FROM `testcasemastertemp` tcm

*/


