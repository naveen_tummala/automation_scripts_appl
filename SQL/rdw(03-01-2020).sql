-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2020 at 10:26 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rdw`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Modified_date` datetime NOT NULL,
  `Active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`id`, `Name`, `Modified_by`, `Modified_date`, `Active`) VALUES
(1, 'RDW', 0, '2019-12-06 14:21:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `browse`
--

CREATE TABLE `browse` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `browse`
--

INSERT INTO `browse` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'common', '2019-09-13 16:43:10', '0000-00-00 00:00:00', 9, 0, 1),
(2, 'skip', '2019-09-13 16:43:10', '0000-00-00 00:00:00', 9, 0, 1),
(6, 'sample test', '2019-09-25 16:06:48', '2019-09-25 10:36:59', 0, 0, 0),
(7, 'Firefox', '2019-10-24 15:41:38', '2019-10-24 11:06:12', 0, 0, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `datasheet`
-- (See below for the actual view)
--
CREATE TABLE `datasheet` (
`testCaseId` int(11)
,`title` varchar(500)
,`TestStep` int(11)
,`Window` varchar(100)
,`DataFields` varchar(255)
,`DataValues` varchar(255)
,`Action` varchar(100)
,`Condition` varchar(100)
,`Browser` varchar(20)
,`FieldName` varchar(100)
,`Comments` varchar(255)
,`AppName` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `data_sheet`
--

CREATE TABLE `data_sheet` (
  `ID` int(11) NOT NULL,
  `TestCaseID` int(11) NOT NULL,
  `TestStep` int(11) NOT NULL,
  `Window` int(11) NOT NULL,
  `object_repo_id` int(11) DEFAULT NULL,
  `DataFields` varchar(255) DEFAULT NULL,
  `test_data_id` int(11) DEFAULT NULL,
  `DataValues` varchar(255) DEFAULT NULL,
  `KeywordID` int(11) NOT NULL,
  `StepCondition` varchar(100) NOT NULL,
  `BrowserID` int(11) NOT NULL,
  `FieldName` varchar(100) NOT NULL,
  `Comments` varchar(255) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_sheet`
--

INSERT INTO `data_sheet` (`ID`, `TestCaseID`, `TestStep`, `Window`, `object_repo_id`, `DataFields`, `test_data_id`, `DataValues`, `KeywordID`, `StepCondition`, `BrowserID`, `FieldName`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 1, 1, 1, 0, '', 0, '', 1, '', 1, '', '', '2019-10-24 20:51:24', '2019-12-06 11:27:22', 1, 9, 1),
(2, 1, 3, 2, 1, '', 1, '', 2, '', 1, 'SubSciber User Name', '', '2019-10-24 20:52:02', '2019-10-29 10:15:11', 1, 0, 1),
(3, 1, 4, 2, 2, '', 2, '', 2, '', 1, 'SubSciber Password', '', '2019-10-24 20:52:34', '2019-10-29 10:24:45', 1, 0, 1),
(4, 1, 5, 2, 3, '', 0, '', 3, '', 1, 'SubSciber Login', '', '2019-10-24 20:53:10', '2019-10-29 10:24:40', 1, 0, 1),
(5, 1, 6, 3, 4, '', 0, '', 3, '', 1, 'Oro Signin', '', '2019-10-24 20:53:47', '2019-10-29 10:24:37', 1, 0, 1),
(6, 1, 7, 3, 5, '', 3, '', 2, '', 1, 'Oro Email Address', '', '2019-10-24 20:55:21', '2019-10-29 10:24:12', 1, 0, 1),
(7, 1, 8, 3, 6, '', 4, '', 2, '', 1, 'Oro Password', '', '2019-10-24 20:56:02', '2019-10-29 10:24:09', 1, 0, 1),
(8, 1, 9, 3, 7, '', 0, '', 3, '', 1, 'Oro Submit ', '', '2019-10-24 20:56:35', '2019-10-29 10:15:29', 1, 0, 1),
(9, 1, 10, 4, 8, '', 0, 'Explore @', 2, '', 1, 'Search', '', '2019-10-24 20:57:18', '2019-10-29 13:20:28', 1, 2, 1),
(10, 1, 11, 4, 9, '', 0, '', 3, '', 1, 'Search button', '', '2019-10-24 20:58:37', '2019-10-29 10:15:23', 1, 0, 1),
(11, 1, 12, 5, 0, '//a[@id=\'abstract_link1\']', 0, '', 4, '', 1, 'search result', '', '2019-10-24 21:00:13', '2019-12-06 11:52:47', 1, 0, 1),
(12, 1, 13, 5, 0, '//a[@id=\'abstract_link1\']', 0, '', 5, '', 1, 'search result', '', '2019-10-24 21:01:03', '2019-12-06 11:52:47', 1, 0, 1),
(13, 1, 2, 3, 0, 'XPH//*[@id=\'signInExpandControls\']', 0, '', 3, '', 1, 'Signin Expand Controls', '', '2019-10-29 15:43:56', '2019-12-06 11:27:22', 2, 2, 1),
(14, 2, 1, 1, 0, '', 0, '', 1, '', 1, 'Home Page', '', '2019-10-29 16:59:55', '2019-12-06 09:32:18', 2, 2, 1),
(15, 2, 0, 2, 1, '', 1, '', 2, '', 1, 'SubSciber User Name', '', '2019-10-29 17:01:38', '2019-10-29 13:08:48', 2, 0, 0),
(16, 2, 2, 1, 0, 'XPH//*[@id=\'signInExpandControls\']', 0, '', 3, '', 1, 'Signin Expand Controls', '', '2019-10-29 17:05:41', '2019-12-06 09:32:18', 2, 2, 1),
(17, 2, 3, 2, 1, '', 1, '', 2, '', 1, 'SubSciber User Name', '', '2019-10-29 17:07:58', '2019-10-29 13:18:10', 2, 2, 1),
(18, 2, 4, 2, 2, '', 2, '', 2, '', 1, 'SubSciber Password', '', '2019-10-29 17:08:38', '2019-10-29 13:18:23', 2, 2, 1),
(19, 2, 5, 2, 3, '', 0, '', 3, '', 1, 'SubSciber Login', '', '2019-10-29 17:55:48', '2019-10-29 13:18:36', 2, 2, 1),
(20, 2, 6, 3, 4, '', 0, '', 3, '', 1, 'Oro Signin', '', '2019-10-29 18:40:51', '2019-10-29 13:18:46', 2, 2, 1),
(21, 2, 7, 3, 5, '', 3, '', 2, '', 1, 'Oro Email Address', '', '2019-10-29 18:42:04', '2019-10-29 13:18:57', 2, 2, 1),
(22, 2, 8, 3, 6, '', 4, '', 2, '', 1, 'Oro Password', '', '2019-10-29 18:42:41', '2019-10-29 13:19:10', 2, 2, 1),
(23, 2, 9, 3, 7, '', 0, '', 3, '', 1, 'Oro Submit ', '', '2019-10-29 18:43:28', '2019-10-29 13:19:22', 2, 2, 1),
(24, 2, 10, 4, 8, '', 0, 'Explore  RISK tes', 2, '', 1, 'Search', '', '2019-10-29 18:44:44', '2019-10-29 13:19:34', 2, 2, 1),
(25, 2, 11, 4, 9, '', 0, '', 3, '', 1, 'Search button', '', '2019-10-29 18:45:48', '2019-10-29 13:19:45', 2, 2, 1),
(26, 2, 12, 5, 0, '//a[@id=\'abstract_link1\']', 0, '', 4, '', 1, 'Search result', '', '2019-10-29 18:46:29', '2019-10-29 13:19:58', 2, 2, 1),
(27, 2, 13, 5, 0, '//a[@id=\'abstract_link1\']', 0, '', 5, '', 1, 'Search result', '', '2019-10-29 18:47:04', '2019-10-29 13:20:05', 2, 2, 1),
(28, 1, 14, 1, 7, '', 0, 'fdgdfg', 3, 'sample condition', 1, '', '', '2019-12-06 17:14:43', '2019-12-06 11:53:01', 1, 0, 0),
(29, 1, 15, 3, 7, '', 0, 'asdfsdf', 3, '', 1, '', '', '2019-12-10 12:50:27', '2019-12-11 11:40:06', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'CLEARSESSION', '2019-10-24 20:05:15', '2019-10-24 14:35:15', 0, 0, 1),
(2, 'ENTER', '2019-10-24 20:05:25', '2019-10-24 14:35:25', 0, 0, 1),
(3, 'CLICK', '2019-10-24 20:05:35', '2019-12-06 11:26:06', 0, 0, 1),
(4, 'SPECIFICWAIT', '2019-10-24 20:05:44', '2019-10-24 14:35:44', 0, 0, 1),
(5, 'ISDISPLAYED', '2019-10-24 20:05:52', '2019-10-24 14:35:52', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created_date` datetime DEFAULT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'ORO', '2019-10-24 20:04:20', '2019-10-24 14:34:20', 0, 0, 1),
(2, 'Sample', '2019-12-06 16:34:28', '2019-12-06 11:04:39', 0, 0, 0),
(3, 'OSO', '2019-12-11 16:49:47', '2019-12-11 11:19:53', 0, 0, 0),
(4, 'OSO', '2019-12-11 16:51:16', '2019-12-11 11:21:16', 0, 0, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `objectrepo`
-- (See below for the actual view)
--
CREATE TABLE `objectrepo` (
`ObjectType` varchar(20)
,`ObjectName` varchar(100)
,`ObjectLocator` varchar(200)
,`Comments` varchar(500)
,`AppName` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `object_repo`
--

CREATE TABLE `object_repo` (
  `ID` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Locator` varchar(200) NOT NULL,
  `Comments` varchar(500) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `object_repo`
--

INSERT INTO `object_repo` (`ID`, `Type`, `Name`, `Locator`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 12, 'SubLoginEmail', '//input[@id=\'username\']', '', '2019-10-24 20:12:25', '2019-10-24 15:04:20', 0, 0, 1),
(2, 12, 'SubLoginPassword', '//input[@id=\'password\']', '', '2019-10-24 20:12:39', '2019-10-24 15:02:58', 0, 0, 1),
(3, 13, 'SubLoginButton', '//input[@id=\'usernameSubmit\']', '', '2019-10-24 20:12:57', '2019-10-24 15:04:04', 0, 0, 1),
(4, 14, 'SignIn', '//a[@id=\'signIn\']', '', '2019-10-24 20:13:08', '2019-10-24 15:02:29', 0, 0, 1),
(5, 12, 'EmailAddress', '//input[@id=\'username\']', '', '2019-10-24 20:13:21', '2019-10-24 15:03:33', 0, 0, 1),
(6, 12, 'Password', '//input[@id=\'password\']', '', '2019-10-24 20:13:35', '2019-10-24 15:02:41', 0, 0, 1),
(7, 13, 'Submit', '//input[@id=\'submit\']', '', '2019-10-24 20:13:50', '2019-10-24 15:01:51', 0, 0, 1),
(8, 12, 'SearchInput', '//input[@id=\'q\']', '', '2019-10-24 20:14:05', '2019-10-24 15:02:03', 0, 0, 1),
(9, 13, 'SearchButton', '//input[@id=\'searchBtn\']', '', '2019-10-24 20:14:20', '2019-10-24 15:02:14', 0, 0, 1),
(10, 14, 'HeaderSubject', '//*[@id=\'homenav\']//*[contains(text(), \'Subject\')]', '', '2019-10-24 20:14:31', '2019-10-24 15:03:16', 0, 0, 1),
(11, 14, 'BrowserAll', '//*[@id=\'ORONavigation\']//*[text()=\'Browse All\']', '', '2019-10-24 20:14:44', '2019-10-24 15:03:50', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `object_types`
--

CREATE TABLE `object_types` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `object_types`
--

INSERT INTO `object_types` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'SubLoginEmail', '2019-10-24 20:06:05', '2019-10-24 15:04:59', 0, 0, 0),
(2, 'SubLoginPassword', '2019-10-24 20:06:13', '2019-10-24 15:05:02', 0, 0, 0),
(3, 'SubLoginButton', '2019-10-24 20:06:21', '2019-10-24 15:04:55', 0, 0, 0),
(4, 'SignIn', '2019-10-24 20:06:27', '2019-10-24 15:04:52', 0, 0, 0),
(5, 'EmailAddress', '2019-10-24 20:06:35', '2019-10-24 15:04:29', 0, 0, 0),
(6, 'Password', '2019-10-24 20:06:43', '2019-10-24 15:04:41', 0, 0, 0),
(7, 'Submit', '2019-10-24 20:06:49', '2019-10-24 15:04:34', 0, 0, 0),
(8, 'SearchInput', '2019-10-24 20:06:57', '2019-10-24 15:04:48', 0, 0, 0),
(9, 'SearchButton', '2019-10-24 20:07:05', '2019-10-24 15:04:44', 0, 0, 0),
(10, 'HeaderSubject', '2019-10-24 20:07:21', '2019-10-24 15:04:37', 0, 0, 0),
(11, 'BrowserAll', '2019-10-24 20:07:24', '2019-10-24 15:04:25', 0, 0, 0),
(12, 'TXTobj', '2019-10-24 20:30:52', '2019-10-24 15:00:52', 0, 0, 1),
(13, 'BTNobj', '2019-10-24 20:30:59', '2019-10-24 15:00:59', 0, 0, 1),
(14, 'XPHobj', '2019-10-24 20:31:07', '2019-10-24 15:01:07', 0, 0, 1),
(15, 'SLBobj', '2019-10-29 18:55:28', '2019-10-29 13:25:28', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` date NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'Medium', '2019-10-24', '2019-10-24 14:34:38', 0, 0, 1),
(2, 'Complex', '2019-10-29', '2019-10-29 13:24:50', 0, 0, 1),
(3, 'High Complex', '2019-10-29', '2019-10-29 13:24:59', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `Created_date` datetime DEFAULT NULL,
  `Modified_date` datetime DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `template_testdata`
-- (See below for the actual view)
--
CREATE TABLE `template_testdata` (
`testCaseId` int(11)
,`title` varchar(500)
,`TestStep` int(11)
,`Window` varchar(100)
,`DataFields` varchar(255)
,`DataValues` varchar(255)
,`Action` varchar(100)
,`Conditions` varchar(100)
,`Browser` varchar(20)
,`FieldName` varchar(100)
,`Comments` varchar(255)
,`AppName` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `testcasemaster`
-- (See below for the actual view)
--
CREATE TABLE `testcasemaster` (
`TCID` int(11)
,`Title` varchar(500)
,`TestRailCaseNo` varchar(20)
,`priority` varchar(20)
,`owner` varchar(100)
,`AppName` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `testdata`
-- (See below for the actual view)
--
CREATE TABLE `testdata` (
`ParameterName` varchar(100)
,`ParameterValue` varchar(200)
,`Index` int(3)
,`Comments` varchar(500)
,`AppName` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `test_case_master`
--

CREATE TABLE `test_case_master` (
  `ID` int(11) NOT NULL,
  `Title` varchar(500) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `TestRailCaseNo` varchar(20) NOT NULL,
  `PriorityID` int(11) NOT NULL,
  `assignedUserID` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_case_master`
--

INSERT INTO `test_case_master` (`ID`, `Title`, `ModuleID`, `TestRailCaseNo`, `PriorityID`, `assignedUserID`, `status`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'Verify that a wildcard search works', 1, '1', 1, 2, 1, '2019-10-24 20:18:57', '2019-10-29 13:22:48', 1, 2, 1),
(2, 'Verify that a two word search works', 1, '2', 1, 2, 1, '2019-10-29 16:57:35', '2019-10-29 13:22:43', 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_case_status`
--

CREATE TABLE `test_case_status` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_case_status`
--

INSERT INTO `test_case_status` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'Completed', '2019-10-24 20:18:26', '2019-10-24 14:48:26', 0, 0, 1),
(2, 'Pending', '2019-10-24 20:18:39', '2019-10-24 14:48:39', 0, 0, 1),
(3, 'In-Progress', '2019-10-29 18:56:19', '2019-10-29 13:26:19', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_data`
--

CREATE TABLE `test_data` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Value` varchar(200) NOT NULL,
  `ParamIndex` int(3) NOT NULL,
  `Comments` varchar(500) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_data`
--

INSERT INTO `test_data` (`ID`, `Name`, `Value`, `ParamIndex`, `Comments`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'SubUserName', 'SafariSpoof', 1, '', '2019-10-24 20:16:16', '2019-10-24 14:46:16', 0, 0, 1),
(2, 'SubPassword', 'cavexf', 2, '', '2019-10-24 20:16:32', '2019-10-24 14:46:32', 0, 0, 1),
(3, 'OroLoginEmail', 'anpoju.chary@valuelabs.com', 3, '', '2019-10-24 20:16:54', '2019-10-24 14:46:54', 0, 0, 1),
(4, 'OroLoginPassword', 'password', 4, '', '2019-10-24 20:17:09', '2019-10-24 14:47:09', 0, 0, 1),
(5, 'SubUserName', 'SafariSpoof', 5, '', '2019-10-24 20:17:25', '2019-10-24 14:47:25', 0, 0, 1),
(6, 'LoginEmail', 'ananthalakshmi.inguva@valuelabs.com', 6, '', '2019-10-24 20:17:48', '2019-10-24 14:47:48', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `Created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Name`, `username`, `password`, `role_id`, `Created_date`, `Modified_date`, `active`, `created_by`, `modified_by`) VALUES
(1, 'Haribabu', 'haribabu.chereddy@valuelabs.com', 'welcome', 6, '2019-09-16 16:59:08', '2019-10-04 10:32:09', 1, 0, 0),
(2, 'Anil', 'anil.komma@valuelabs.com', 'welcome@123', 7, '2019-09-13 16:48:04', '2019-10-04 10:32:09', 1, 0, 0),
(3, 'Navya', 'navya.kutcharlapati@valuelabs.com', 'welcome@123', 8, '2019-09-13 16:48:16', '2019-10-04 10:32:09', 0, 0, 0),
(4, 'Srilatha', 'srilatha.amberkar@valuelabs.com', 'welcome@123', 7, '2019-09-13 16:48:19', '2019-10-04 10:32:09', 1, 0, 0),
(5, 'Rupesh', 'rupesh.bramandlapalli@valuelabs.com', 'welcome@123', 8, '2019-09-13 16:48:22', '2019-10-04 10:32:09', 0, 0, 0),
(6, 'Srikanth', 'srikanth.thumukuntla@valuelabs.com', 'welcome@123', 2, '2019-09-13 16:48:26', '2019-10-04 10:32:09', 1, 0, 0),
(7, 'Santhosh', 'santhosh.gundapu@valuelabs.com', 'welcome@123', 8, '2019-09-13 16:48:29', '2019-10-04 10:32:09', 0, 0, 0),
(8, 'Tharun', 'tharun.jonnadula@valuelabs.com', 'welcome@123', 1, '2019-09-13 16:48:35', '2019-10-04 10:32:09', 1, 0, 0),
(9, 'Naveen', 'naveen.tummala@valuelabs.com', 'welcome@123', 1, '2019-09-17 19:46:04', '2019-10-04 10:32:09', 1, 0, 0),
(13, 'Sarath', 'sarath.jampani@valuelabs.com', 'welcome', 8, '2019-10-25 15:23:20', '2019-10-25 09:53:27', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `window`
--

CREATE TABLE `window` (
  `ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Created_date` datetime NOT NULL,
  `Modified_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Created_by` int(11) NOT NULL,
  `Modified_by` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `window`
--

INSERT INTO `window` (`ID`, `Name`, `Created_date`, `Modified_date`, `Created_by`, `Modified_by`, `Active`) VALUES
(1, 'RDW_HomePage', '2019-10-24 20:15:18', '2019-10-24 14:45:18', 0, 0, 1),
(2, 'RDW_Subscriber\'s loginPage', '2019-10-24 20:15:25', '2019-10-24 14:45:25', 0, 0, 1),
(3, 'RDW_OroLogin', '2019-10-24 20:15:34', '2019-10-24 14:45:34', 0, 0, 1),
(4, 'RDW_Search', '2019-10-24 20:15:41', '2019-10-24 14:45:41', 0, 0, 1),
(5, 'RDW_SearchResult', '2019-10-24 20:15:49', '2019-10-24 14:45:49', 0, 0, 1);

-- --------------------------------------------------------

--
-- Structure for view `datasheet`
--
DROP TABLE IF EXISTS `datasheet`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datasheet`  AS  select `dt`.`TestCaseID` AS `testCaseId`,`tcm`.`Title` AS `title`,`dt`.`TestStep` AS `TestStep`,`wn`.`Name` AS `Window`,case when ceiling(`dt`.`object_repo_id`) <> 0 then concat(`objt`.`Name`,':',`objr`.`Name`) else `dt`.`DataFields` end AS `DataFields`,case when ceiling(`dt`.`test_data_id`) <> 0 then concat('dt:',`td`.`Name`,'#',`td`.`ParamIndex`) else `dt`.`DataValues` end AS `DataValues`,`kw`.`Name` AS `Action`,`dt`.`StepCondition` AS `Condition`,`br`.`Name` AS `Browser`,`dt`.`FieldName` AS `FieldName`,`dt`.`Comments` AS `Comments`,(select `application`.`Name` from `application` where `application`.`id` = 1) AS `AppName` from (((((((`data_sheet` `dt` join `test_case_master` `tcm` on(`tcm`.`ID` = `dt`.`TestCaseID`)) left join `keywords` `kw` on(`kw`.`ID` = `dt`.`KeywordID`)) left join `window` `wn` on(`wn`.`ID` = `dt`.`Window`)) left join `object_repo` `objr` on(`objr`.`ID` = `dt`.`object_repo_id`)) left join `object_types` `objt` on(`objt`.`ID` = `objr`.`Type`)) left join `test_data` `td` on(`td`.`ID` = `dt`.`test_data_id`)) left join `browse` `br` on(`br`.`ID` = `dt`.`BrowserID`)) where `dt`.`Active` = 1 order by `dt`.`TestCaseID`,`dt`.`TestStep` ;

-- --------------------------------------------------------

--
-- Structure for view `objectrepo`
--
DROP TABLE IF EXISTS `objectrepo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `objectrepo`  AS  select `ot`.`Name` AS `ObjectType`,`obr`.`Name` AS `ObjectName`,`obr`.`Locator` AS `ObjectLocator`,`obr`.`Comments` AS `Comments`,(select `application`.`Name` from `application` where `application`.`id` = 1) AS `AppName` from (`object_repo` `obr` left join `object_types` `ot` on(`ot`.`ID` = `obr`.`Type`)) where `obr`.`Active` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `template_testdata`
--
DROP TABLE IF EXISTS `template_testdata`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `template_testdata`  AS  select `dt`.`TestCaseID` AS `testCaseId`,`tcm`.`Title` AS `title`,`dt`.`TestStep` AS `TestStep`,`wn`.`Name` AS `Window`,case when ceiling(`dt`.`object_repo_id`) <> 0 then concat(`objt`.`Name`,':',`objr`.`Name`) else `dt`.`DataFields` end AS `DataFields`,case when ceiling(`dt`.`test_data_id`) <> 0 then concat('dt:',`td`.`Name`,'#',`td`.`ParamIndex`) else `dt`.`DataValues` end AS `DataValues`,`kw`.`Name` AS `Action`,`dt`.`StepCondition` AS `Conditions`,`br`.`Name` AS `Browser`,`dt`.`FieldName` AS `FieldName`,`dt`.`Comments` AS `Comments`,(select `application`.`Name` from `application` where `application`.`id` = 1) AS `AppName` from (((((((`data_sheet` `dt` join `test_case_master` `tcm` on(`tcm`.`ID` = `dt`.`TestCaseID`)) left join `keywords` `kw` on(`kw`.`ID` = `dt`.`KeywordID`)) left join `window` `wn` on(`wn`.`ID` = `dt`.`Window`)) left join `object_repo` `objr` on(`objr`.`ID` = `dt`.`object_repo_id`)) left join `object_types` `objt` on(`objt`.`ID` = `objr`.`Type`)) left join `test_data` `td` on(`td`.`ID` = `dt`.`test_data_id`)) left join `browse` `br` on(`br`.`ID` = `dt`.`BrowserID`)) where `dt`.`Active` = 0 ;

-- --------------------------------------------------------

--
-- Structure for view `testcasemaster`
--
DROP TABLE IF EXISTS `testcasemaster`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `testcasemaster`  AS  select `tcm`.`ID` AS `TCID`,`tcm`.`Title` AS `Title`,`tcm`.`TestRailCaseNo` AS `TestRailCaseNo`,`p`.`Name` AS `priority`,`u`.`Name` AS `owner`,(select `application`.`Name` from `application` where `application`.`id` = 1) AS `AppName` from ((`test_case_master` `tcm` join `priority` `p` on(`p`.`ID` = `tcm`.`PriorityID`)) join `users` `u` on(`u`.`id` = `tcm`.`assignedUserID`)) where `tcm`.`Active` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `testdata`
--
DROP TABLE IF EXISTS `testdata`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `testdata`  AS  select `test_data`.`Name` AS `ParameterName`,`test_data`.`Value` AS `ParameterValue`,`test_data`.`ParamIndex` AS `Index`,`test_data`.`Comments` AS `Comments`,(select `application`.`Name` from `application` where `application`.`id` = 1) AS `AppName` from `test_data` where `test_data`.`Active` = 1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `browse`
--
ALTER TABLE `browse`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `data_sheet`
--
ALTER TABLE `data_sheet`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `object_repo`
--
ALTER TABLE `object_repo`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `object_types`
--
ALTER TABLE `object_types`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_case_master`
--
ALTER TABLE `test_case_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `test_case_status`
--
ALTER TABLE `test_case_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `test_data`
--
ALTER TABLE `test_data`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `window`
--
ALTER TABLE `window`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `browse`
--
ALTER TABLE `browse`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_sheet`
--
ALTER TABLE `data_sheet`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `object_repo`
--
ALTER TABLE `object_repo`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `object_types`
--
ALTER TABLE `object_types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test_case_master`
--
ALTER TABLE `test_case_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `test_case_status`
--
ALTER TABLE `test_case_status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_data`
--
ALTER TABLE `test_data`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `window`
--
ALTER TABLE `window`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
